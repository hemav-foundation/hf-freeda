from dronekit import *

class AutopilotInterface():

    def __init__(self, vehicle):
        
        self.vehicle = vehicle
        print('class initialized')

########## GET INFORMATION FROM DRONE METHODS

    def set_data_drone(self):      # necessary data to tag the obtained vegetated images
        latitude = self.vehicle.location.global_frame.lat
        longitude = self.vehicle.location.global_frame.lon
        heading = self.vehicle.heading
        altitude = self.vehicle.location.global_frame.alt

        data_drone = [latitude, longitude, heading, altitude]

        return data_drone

    def image_coordinates(self):
        latitude = self.vehicle.location.global_frame.lat
        longitude = self.vehicle.location.global_frame.lon
        heading = self.vehicle.heading
        pitch = self.vehicle.attitude.pitch
        altitude = self.vehicle.location.global_frame.alt
        roll = self.vehicle.attitude.roll

        coordinates = [latitude, longitude]
        tag_images = [coordinates, heading, altitude, pitch, roll]

        return tag_images
    
    def get_airspeed(self):
        return self.vehicle.airspeed
    
    def get_groundspeed(self):
        return self.vehicle.groundspeed

    def get_coordinates(self):
        latitude = self.vehicle.location.global_frame.lat
        longitude = self.vehicle.location.global_frame.lon

        coordinates = [latitude, longitude]
        return coordinates

    def get_altitude(self):
        return self.vehicle.location.global_relative_frame.alt


    def get_armed(self):
        return self.vehicle.armed

    # # #

    def get_latitude(self):
        #print('##### autopilot', self.vehicle.location.global_frame.lat)
        return self.vehicle.location.global_frame.lat

    # # #

    def get_longitude(self):
        return self.vehicle.location.global_frame.lon

    # # #

    def get_heading(self):
        return self.vehicle.heading

    # # #

    def get_yaw(self):  # pan
        return self.vehicle.attitude.yaw

    def get_pitch(self):  # tilt

        try:
            print(self.vehicle.attitude.pitch)

        except:
            print("not able to get pitch")

        return self.vehicle.attitude.pitch

    # # #

    def get_roll(self):  # roll

        try:
            print(self.vehicle.attitude.roll)
        except:
            print("not able to get roll")

        return self.vehicle.attitude.roll

########## CALLIBRATION METHODS

    def get_voltage(self):
        print('Battery voltage:', self.vehicle.battery.voltage)
        return self.vehicle.battery.voltage

    def get_compass_1(self):
        offset_compass_1_x = self.vehicle.parameters['COMPASS_OFS_X']
        offset_compass_1_y = self.vehicle.parameters['COMPASS_OFS_Y']
        offset_compass_1_z = self.vehicle.parameters['COMPASS_OFS_Z']

        compass_1_type = int(self.vehicle.parameters['COMPASS_EXTERNAL'])

        compass_1_use = int(self.vehicle.parameters['COMPASS_USE'])

        compass1 = [offset_compass_1_x, offset_compass_1_y, offset_compass_1_z]
        return compass1, compass_1_type, compass_1_use

    def get_compass_2(self):
        offset_compass_2_x = self.vehicle.parameters['COMPASS_OFS2_X']
        offset_compass_2_y = self.vehicle.parameters['COMPASS_OFS2_Y']
        offset_compass_2_z = self.vehicle.parameters['COMPASS_OFS2_Z']

        compass_2_type = int(self.vehicle.parameters['COMPASS_EXTERN2'])
        compass_2_use = int(self.vehicle.parameters['COMPASS_USE2'])

        compass2 = [offset_compass_2_x, offset_compass_2_y, offset_compass_2_z]
        return compass2, compass_2_type, compass_2_use
    
    def get_compass_3(self):
        offset_compass_3_x = self.vehicle.parameters['COMPASS_OFS3_X']
        offset_compass_3_y = self.vehicle.parameters['COMPASS_OFS3_Y']
        offset_compass_3_z = self.vehicle.parameters['COMPASS_OFS3_Z']

        compass_3_type = int(self.vehicle.parameters['COMPASS_EXTERN3'])
        compass_3_use = int(self.vehicle.parameters['COMPASS_USE3'])

        compass3 = [offset_compass_3_x, offset_compass_3_y, offset_compass_3_z]
        return compass3, compass_3_type, compass_3_use
    
    def disable_compass(self, num):
        disabled = False
        if num is 1:
            action = 'COMPASS_USE'
        else:
            action = 'COMPASS_USE' + str(num)
        
        compass_1_use = int(self.vehicle.parameters['COMPASS_USE'])
        compass_2_use = int(self.vehicle.parameters['COMPASS_USE2'])
        compass_3_use = int(self.vehicle.parameters['COMPASS_USE3'])

        print('compass 1 use:', compass_1_use)
        print('compass 2 use:', compass_2_use)
        print('compass 3 use:', compass_3_use)

        if compass_1_use is 1 and compass_2_use is 1 and compass_3_use is 1:
            self.vehicle.parameters[action] = 0
            print('compass disabled')
            disabled = True

        return disabled

    def vehicle_armable(self):
        return self.vehicle.is_armable
        
    def airspeed_calibration(self):
        self.vehicle.send_calibrate_barometer()
    
    def gps_info(self):
        satellites = self.vehicle.gps_0.satellites_visible
        
        return satellites

    def get_ekf(self):
        return self.vehicle.ekf_ok
    
    def do_land_start(self, loiterWp):
        print('Do land start order')
        startLanding = self.vehicle.message_factory.command_long_encode(
            0, 0,  # target_system, target_component
            mavutil.mavlink.MAV_CMD_DO_JUMP,  # .
            0,  # confirmation
            loiterWp,  # param 1, The command index/sequence number of the command to jump to
            0,  # param 2, Number of times that the DO_JUMP command will execute before moving to the next sequential command. If the value is zero the next command will execute immediately. A value of -1 will cause the command to repeat indefinitely.
            0, 0, 0, 0, 0,  # param 3-7, Not applying for this command
        )
        self.vehicle.send_mavlink(startLanding)


