'''
HEMAV FOUNDATION
10 / 05 / 2019
TEST 1

We will test the reliability of the image processing with basic targets
(simple color detection) and the positioning of the target in global coordinates.

The test will consist in the aircraft controlled by the pilot flying over
a static target with fixed and knowed coordinates. This coordinates will be
saved in a file to see the noise in the detection of the target.
As the target is static, all de coordinates in the text should be the same or
very similar.
'''

import thread # Multi-threading library
import time
from dronekit import connect, VehicleMode, LocationGlobal, LocationGlobalRelative
from pymavlink import mavutil # Needed for command message definitions
import math
import constants
from coordinatesFromPhoto import coordinatesFromPhoto
from autoDef import detection

newDetection = 0
cX = 0
cY = 0

def rad2deg(radians):
    return radians * 180.0 / math.pi

def deg2rad(degrees):
    return degrees * math.pi / 180.0

def imageProcessingLoop():
    global newDetection, cX, cY
    while True:
        time.sleep(0.01)
        (cX, cY, newDetection) = detection()

connectionString = "/dev/ttyACM0"

print("Connecting to vehicle")
vehicle = connect(connectionString, wait_ready = True)


@vehicle.on_message('RC_CHANNELS')
def listener(self, name, message):
    global ch6
    ch6 = message.chan6_raw


@vehicle.on_message('ATTITUDE')
def listener(self, name, message):
    global yawaRad, rollRad, pitchRad
    yawRad = message.yaw
    rollRad = message.roll
    pitchRad = message.pitch

time.sleep(2)

thread.start_new_thread(imageProcessingLoop, ())

i = 1
file = open("targetCoordinates.txt", "w")

while True:
    if(ch6 > 1500):
        if(newDetection == 1):
            boatLocation = coordinatesFromPhoto(rad2deg(pitch), rad2deg(roll), yaw, cX, cY, vehicle.location.global_relative_frame)
            file.write("Index: %i\tLat: %f\tLon: %f\n" %(i, boatLocation.lat, boatLocation.lon))
            i = i + 1
            newDetection = 0
