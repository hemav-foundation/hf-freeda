from __future__ import print_function
from math import asin,cos,pi,sin

#from dronekit import connect, VehicleMode, LocationGlobalRelative, LocationGlobal, Command
from dronekit import *
import time
import math
from pymavlink import mavutil
from commonFunctions import *
from config import *
from flights import *
import sys

distance = None
widthRectangle = None
spaceDistance = None
spaceBtwLines = None
height = None
inverse = True
headingOffset = None
latFlight = None
lonFlight = None
headingFlight = None

distance = float(sys.argv[1]) / 1000
widthRectangle = float(sys.argv[2]) / 1000
spaceDistance = float(sys.argv[3]) / 1000
spaceBtwLines = float(sys.argv[4]) / 1000
height = int(sys.argv[5])
headingOffset = int(sys.argv[7])

sitl = None

if( int(sys.argv[6]) == 1):
    inverse = True
else:
    inverse = False


if connectionString != "local":
    connection_string = flight_controller['port']
    latFlight = float(sys.argv[8])
    lonFlight = float(sys.argv[9])
    headingFlight = int(sys.argv[10])

else:
    connection_string = None
    latFlight = -35.363261
    lonFlight = 149.165229
    headingFlight = 353


#Start SITL if no connection string specified
if not connection_string:
    import dronekit_sitl
    sitl = dronekit_sitl.start_default()
    connection_string = sitl.connection_string()


# Connect to the Vehicle. 
#   Set `wait_ready=True` to ensure default attributes are populated before `connect()` returns.
#print("\nConnecting to vehicle on: %s" % connection_string)

vehicle = connect(connection_string, baud=flight_controller['baudrate'], wait_ready=True)


# Get some vehicle attributes (state)
global cmds

cmds = vehicle.commands

latWind = vehicle.location.global_frame.lat
lonWind = vehicle.location.global_frame.lon
headingWind = vehicle.heading 

#rectangleMission can change between reversed or normal depending how you want to make the mission

if inverse == False:
    cmds = rectangleMission_normal(latWind, lonWind, headingWind, distance, spaceDistance, widthRectangle, spaceBtwLines, height, headingOffset, latFlight, lonFlight, headingFlight, cmds)
else:
    print('reversed')
    cmds = rectangleMission_reversed(latWind, lonWind, headingWind, distance, spaceDistance, widthRectangle, spaceBtwLines, height, headingOffset, latFlight, lonFlight, headingFlight, cmds)


print(" Upload new commands to vehicle")


if connectionString == "local":
    save_mission('./mission.waypoints', cmds)

mission_values = {
    "distance": "",
    "width": "",
    "spaceDistance": "",
    "spaceLines": "",
    "height": "",
    "inverse": "",
    "headingOffset": "",
    "latFlight": "", 
    "lonFlight": "", 
    "headingFlight": "", 
    "missionParameters": "",
}

mission_values = edit_mission_values(mission_values, distance, spaceDistance, widthRectangle, spaceBtwLines, height, headingOffset, latFlight, lonFlight, headingFlight)
edit_mission_json(mission_values)

# Close vehicle object before exiting script
vehicle.close()

# Shut down simulator
if sitl is not None:
    sitl.stop()



