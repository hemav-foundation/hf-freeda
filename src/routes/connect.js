const { Router } = require("express");
var { PythonShell } = require("python-shell");
const router = Router();
var Config = require('./config');
const environment = Config.environment;
const checkDiskSpace = require('check-disk-space');
const rimraf = require('rimraf')
const fs = require('fs')

class LocationDrone {
    constructor(heading, lat, lon, alt) {
        this.heading = heading;
        this.lon = lon;
        this.lat = lat;
        this.alt = alt;
    }
}

function round(value, decimals) {
    return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
}

function autoDelete() {
    checkDiskSpace('/mnt').then(async(disk) => {

        let freeSpace = round(disk.free / 1073741824, 2)
        let sizeSpace = round(disk.size / 1073741824, 2)
        let diskPercentage = 100 - ((freeSpace / sizeSpace) * 100)

        if (diskPercentage >= 70) {
            let jsonSD = JSON.parse(fs.readFileSync('results.json'))
            var pointer = 0

            for (let index = 0; index < 6; index++) {
                if (jsonSD.dataOfFlights.length > 5) {
                    if (jsonSD.dataOfFlights[pointer].saved == 'true') {
                        await rimraf('public/results/photos/' + jsonSD.dataOfFlights[pointer].id_flight, () => {})
                        jsonSD.dataOfFlights.splice(pointer, 1)

                    } else {
                        pointer += 1
                    }
                }
            }
            fs.writeFileSync('./results.json', JSON.stringify(jsonSD))

        } else {
            console.log('not yet');
        }

    })
}

router.get("/", (req, res) => {
    let options;

    if (environment == "drone" || environment == "linux") {
        options = {
            mode: "text",
            pythonPath: "/usr/bin/python3",
            pythonOptions: ["-u"], // get print results in real-time
            scriptPath: "./scripts"
        };
    } else if (environment == "win") {
        options = {
            mode: "text",
            pythonOptions: ["-u"], // get print results in real-time
            scriptPath: "./scripts"
        };
    } else {
        options = {
            mode: "text",
            pythonPath: "/usr/local/bin/python3",
            pythonOptions: ["-u"], // get print results in real-time
            scriptPath: "./scripts"
        };
    }

    PythonShell.run("connect.py", options, function(err, results) {
        //if (err) throw err;
        if (err) {
            res.status(400).send({ message: "ERROR: Fallo el script connect.py" });
            console.log(err);
        } else {
            location = new LocationDrone(
                results[0],
                results[1],
                results[2],
                results[3]
            );

            res.status(200).send(location);
        }
    });

    autoDelete();

});

router.post("/manualMode", (req, res) => {
    let options;

    if (environment == "drone" || environment == "linux") {
        options = {
            mode: "text",
            pythonPath: "/usr/bin/python3",
            pythonOptions: ["-u"], // get print results in real-time
            scriptPath: "./scripts"
        };
    } else if (environment == "win") {
        options = {
            mode: "text",
            pythonOptions: ["-u"], // get print results in real-time
            scriptPath: "./scripts"
        };
    } else {
        options = {
            mode: "text",
            pythonPath: "/usr/local/bin/python3",
            pythonOptions: ["-u"], // get print results in real-time
            scriptPath: "./scripts"
        };
    }

    PythonShell.run("manualMode.py", options, function(err, results) {
        //if (err) throw err;
        if (err) {
            res.status(400).send({ message: "ERROR: Fallo el script manualMode.py" });
            console.log(err);
        } else {

            res.status(200).send({message: "Manual mode "});
        }
    });

    autoDelete();

});

module.exports = router;