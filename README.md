# HF-FREEDA
Web application for planning fligth missions with the HEMAV HP2 drone. Developed with Node JS, Python and HTML + CSS + jQuery

## Packages needed
- Node JS: the app needs the package manager NPM. For install all the necessary dependencies of the project execute the command `npm install` via terminal in the root directory

- Python: The app needs some crucial packahes managed throught the PIP packages manager. Exactly the app needs the DroneKit SITL package for simulate via software a Drone. For install it it's necessary to execute the command `pip install dronekit-sitl` via terminal.

## Configure the variables for local testing
- Backend

> /src/routes/config.js

**Environment** variable ['drone' for production environment / 'mac' for testing in local with macOs / 'win' for testing in local with Windows / 'linux' for testing in local with linux]

- Frontend

> /public/planner/flightPlanner.html
> /public/results/History.html
> /public/results/viewfinder.html

**ipConecction** variable [‘192.168.0.10’ for production environment / ‘localhost’ for local environment]

> /scripts/config.py

**connectionString** variable [‘drone’ for production environment / ‘local’ for local environment]

## Execute the code

For start the application it is necessary to execute the command `npm start` via terminal in the root directory
