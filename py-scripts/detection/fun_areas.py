from __future__ import division
import cv2
import numpy as np
from time import time
import math
from sklearn.cluster import DBSCAN


img = cv2.imread("inputs/foto.JPG") # sacar foto, mirar desde raspi si no hace falta escribirla

altura = 100 # leer altura de la pixhawk
barco_min = 5 # se puede cambiar si la mision cambiar para detectar personas
barco_max = 20


def areas(altura, barco_min, barco_max):
    from time import time
    tinicial = time()

    # OBJETIVO
    # Area objetivo (m2)
    area_metros_min = barco_min
    area_metros_max = barco_max

    # CAMARA
    # Sensor (mm)
    sx = 3.68
    sy = 2.76
    # Focal length of lens (mm)
    fl = 3.04
    # Pixels
    px = 1920
    py = 1080
    pixels_camera = px*py


    # Field of view wide (gra)
    HFOV = 62.2
    #HFOV = math.radians(HFOV)
    HFOVcal = 2*math.atan(sx/(2*fl))
    #HFOVcal = math.degrees(HFOVcal)
    # Field of view tall (gra)
    VFOV = 48.8
    #VFOV = math.radians(VFOV)
    VFOVcal = 2*math.atan(sy/(2*fl))
    #VFOVcal = math.degrees(VFOVcal)

    # GIMBAL angulo (grados)
    gimbal = 0
    gimbal = math.radians(gimbal)
    # falta otro eje gimbal

    h = altura
    
    if (h<0):
        h = abs(h)
    if (int(h) == 0):
        h = 1
        
    # FOOTPRINT(m)
    fy2 = h*(math.tan(gimbal+0.5*VFOVcal) - math.tan(gimbal-0.5*VFOVcal))
    fx2 = h*(math.tan(gimbal+0.5*HFOVcal) - math.tan(gimbal-0.5*HFOVcal))
    footprint = fy2*fx2

    area_pixels_min = area_metros_min*pixels_camera/footprint
    area_pixels_max = area_metros_max*pixels_camera/footprint
    
    porc_amin= round (area_pixels_min*100/pixels_camera,3)
    disp_amin = 'Area min esperada: ' + repr(porc_amin)+'%'
    print disp_amin

    porc_amax= round (area_pixels_max*100/pixels_camera,3)
    disp_amax = 'Area max esperada: ' + repr(porc_amax)+'%'
    print disp_amax

    tfinal = time()
    time = tfinal - tinicial

    return area_pixels_min, area_pixels_max, time

def color(img):
    from time import time
    tinicial = time()

    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    mask = cv2.inRange(hsv, (84, 0, 0), (134, 255, 255)) # azul (84, 0, 0), (134, 255, 255)

    imask = mask>0
    color = np.zeros_like(img, np.uint8)
    color[imask] = img[imask]
    img_color = color

    tfinal = time()
    time = tfinal - tinicial

    return img_color, time

def proc_cont(img):
    from time import time
    tinicial = time()

    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    blur = cv2.GaussianBlur(gray, (5, 5), 0)

    th, thresh = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
    #thresh = cv2.adaptiveThreshold(blur, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, 29, 8)

    # kernel = np.ones((3,3), np.uint8) #funcion de la altura
    # thresh = cv2.dilate(thresh, kernel, iterations=2) # al reves por _INV
    # thresh = cv2.erode(thresh, kernel, iterations=1)
    _, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    tfinal = time()
    time = tfinal - tinicial

    return contours, hierarchy, time

def size(contours, area_pixels_min, area_pixels_max):
    from time import time
    tinicial = time()

    areamin = []
    for i in contours:
        areamin.append(cv2.contourArea(i))
    j = [i for i in areamin if i >= area_pixels_min]
    pos2 = []
    for i in j:
         pos2.append(areamin.index(i))
    size_cont = [contours[i] for i in pos2]
    areamax = []
    for i in size_cont:
        areamax.append(cv2.contourArea(i))
    j2 = [i for i in areamax if i < area_pixels_max]
    pos3 = []
    for i in j2:
         pos3.append(areamax.index(i))
    size_cont = [size_cont[i] for i in pos3]

    tfinal = time()
    time = tfinal - tinicial

    return size_cont, time

def hierarchy_cont(contours, hierarchy):
    from time import time
    tinicial = time()

    parents = []
    hierarchy = hierarchy[0]
    for i in range(len(hierarchy)):
        if (hierarchy[i, 3] < 0):
            parents.append(contours[i])
    parents_cont = parents

    tfinal = time()
    time = tfinal - tinicial

    return parents_cont, time

def hull(contours):
    from time import time
    tinicial = time()

    hull = []
    for i in range(len(contours)):
        hull.append(cv2.convexHull(contours[i], False))
    hull_cont = hull

    tfinal = time()
    time = tfinal - tinicial

    return hull_cont, time

def centroids(contours):
    from time import time
    tinicial = time()

    coor = []
    if len(contours)==0:
         tfinal = time()
         time = tfinal - tinicial
         return coor, time
    else:
         for i in range (len(contours)):
             M = cv2.moments(contours[i])
             cX = int(M["m10"] / M["m00"])
             cY = int(M["m01"] / M["m00"])
             coordi = (cX,cY)
             coor.append(coordi)

         tfinal = time()
         time = tfinal - tinicial
         return coor, time

def cluster(contours, centroids):
    from time import time
    tinicial = time()

    data = np.array(centroids)
    X = data

    # Compute DBSCAN
    # eps en funcion de la altura
    db = DBSCAN(eps=200, min_samples=3).fit(X)
    core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
    core_samples_mask[db.core_sample_indices_] = True
    labels = db.labels_

    k = -1 #noise
    class_member_mask = (labels == k)

    coor = X[class_member_mask & core_samples_mask]
    coor = X[class_member_mask & ~core_samples_mask]

    coor_list = []
    cluster_cont = []
    for i in range(len(coor)):
        cc = (coor[i][0], coor[i][1])
        coor_list.append(cc)
        index = centroids.index(coor_list[i])
        cluster_cont.append(contours[index])

    tfinal = time()
    time = tfinal - tinicial

    return coor_list, cluster_cont, X, labels, core_samples_mask, time

def circulo(img, contours):
    from time import time
    tinicial = time()

    for i in range(len(contours)):
        color_contours = (255, 0, 0)  # color for contours
        cv2.drawContours(img, contours, i, color_contours, 2, 8)
        (x, y), radius = cv2.minEnclosingCircle(contours[i])
        center = (int(x), int(y))
        radius = int(radius) + 50
        circulo = cv2.circle(img, center, radius, (255, 0, 255), 5)

    tfinal = time()
    time = tfinal - tinicial
    return circulo, time


try:
    t_inicial = time()

    (area_pixels_min, area_pixels_max, t_areas) = areas(altura, barco_min, barco_max)
    #(img, t_color) = color(img) #al haber reflejos puede crear falsos contornos entre el negro y color claro no eliminado
    (contours, hierarchy, t_proc_cont) = proc_cont(img)
    #(parents_cont, t_hierarchy) = hierarchy_cont(contours, hierarchy)
    (size_cont, t_size) = size(contours, area_pixels_min, area_pixels_max)
    (centroids, t_centroids) = centroids(size_cont)
    (coor, cluster_cont, X, labels, core_samples_mask, t_cluster) = cluster(size_cont, centroids)
    (target, t_circulo) = circulo(img, cluster_cont)

    t_final = time()

    print ('Tiempo areas: ' + repr(t_areas) + 's')
    #print ('Tiempo color: ' + repr(t_color) + 's')
    print ('Tiempo proc_cont: ' + repr(t_proc_cont) + 's')
    #print ('Tiempo hierarchy: ' + repr(t_hierarchy) + 's')
    print ('Tiempo size: ' + repr(t_size) + 's')
    print ('Tiempo centroids: ' + repr(t_centroids) + 's')
    print ('Tiempo cluster: ' + repr(t_cluster) + 's')
    print ('Tiempo circulo: ' + repr(t_circulo) + 's')
    print ('Tiempo total: ' + repr(t_final - t_inicial) + 's')

    print "Detected"



except:
    print "No detection"
    cv2.imshow("img", img)
    detection = (0, 0, 0)

print detection








# gray = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
# blur = cv2.GaussianBlur(gray, (5, 5), 0)
# thresh = cv2.adaptiveThreshold(blur, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, 29, 8)
#
# drawing = np.zeros((thresh.shape[0], thresh.shape[1], 3), np.uint8)
# for i in range(len(size_cont)):
#     color_contours = (0,0, 255)  # color for contours
#     cv2.drawContours(drawing, size_cont, i, color_contours, 2, 1)
# img_size_cont = drawing
#
# # drawing2 = np.zeros((thresh.shape[0], thresh.shape[1], 3), np.uint8)
# # for i in range(len(parents_cont)):
# #     color_contours = (0, 0, 255)  # color for contours
# #     cv2.drawContours(drawing2, parents_cont, i, color_contours, 2, 1)
# # img_parents_cont = drawing2
#
# drawing3 = np.zeros((thresh.shape[0], thresh.shape[1], 3), np.uint8)
# for i in range(len(contours)):
#     color_contours = (0, 255, 0)  # color for contours
#     cv2.drawContours(drawing3, contours, i, color_contours, 2, 1)
# img_cont = drawing3
#
#
# drawing4 = np.zeros((thresh.shape[0], thresh.shape[1], 3), np.uint8)
# for i in range(len(cluster_cont)):
#     color_contours = (130, 40, 200)  # color for contours
#     cv2.drawContours(drawing4, cluster_cont, i, color_contours, 2, 1)
# img_cluster = drawing4
#
#
#
#
# cv2.imshow("procesada", img_cont)
# cv2.imshow("size", img_size_cont)
# cv2.imshow("cluster", img_cluster)
# cv2.imshow("target", target)
#
# # cv2.imshow("hierarchy", img_parents_cont)
#
#
#
#
# import matplotlib.pyplot as plt
# data = np.array(centroids)
# width = 1920
# height = 1080
#
# plt.figure(1)
# plt.plot(data.T[0], data.T[1], 'bo')
# frame = plt.gca()
# frame.axes.get_xaxis().set_visible(False)
# frame.axes.get_yaxis().set_visible(False)
# plt.axis([0, width, height, 0])
#
# k = -1 #noise
# # Black used for noise.
# col = [0, 0, 0, 1]
#
# class_member_mask = (labels == k)
#
# plt.figure(2)
# xy = X[class_member_mask & core_samples_mask]
# plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
#          markeredgecolor='k', markersize=14)
# frame = plt.gca()
# frame.axes.get_xaxis().set_visible(False)
# frame.axes.get_yaxis().set_visible(False)
# plt.axis([0, width, height, 0])
#
# xy = X[class_member_mask & ~core_samples_mask]
# plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
#          markeredgecolor='k', markersize=6)
# frame = plt.gca()
# frame.axes.get_xaxis().set_visible(False)
# frame.axes.get_yaxis().set_visible(False)
# plt.axis([0, width, height, 0])
# plt.show()



cv2.waitKey(0)
cv2.destroyAllWindows()


