global connectionString
global mission_config   # periscope, straight, rectangle, zigzag
global inverse
global visual_camera_settings, monospectral_camera_settings, flight_controller


drone_id = 'HP2-087'
connectionString = "drone" # 'local' for testing in localhost // 'drone' for testing in the raspi
inverse = False


# visual_camera_settings = dict(
#     port = '/dev/video1', 
#     name = 'elp',   #write here the name of the visual camera: elp or arducam
#     frame_width = 3264, 
#     frame_height = 2448,    
#     auto_exposure = 3,
#     brightness = -17,
#     contrast = 0,
#     saturation = 75,
#     light_compensation = 36,
#     frequency = 2,
#     white_balance = 4600,
#     gamma = 100,
#     sharpness = 5,
#     focus_auto = 0,
#     focus_absolute = 120,
#     hue = 200,
#     timer = 10, # Timer in seconds to trigger the camera
# )

# monospectral_camera_settings = dict(
#     frame_width = 2528, 
#     frame_height = 1968,
#     redAWB = 0.9,
#     blueAWB = 2.2,
#     awb_mode = 'off',
#     brightness = 30,
#     exposure_mode = 'auto',
#     drc_strength = 'high',
#     timer = 6, 
# )

flight_controller = dict(
    port = '/dev/serial0',
    baudrate = 921600,
)

rockblock_settings = dict(
    port = '/dev/ttyUSB0',
   #gcs_sn = "0201298", # Hemav-Tester rockBlock
    baudrate = 19200,
    message_timer = 60, # Send message timer in seconds
    landing_timer = 40,
)

mission_settings = dict(
    
)

