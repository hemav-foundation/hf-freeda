#include <iostream>
#include <fstream>
#include <math.h>
#include <stdlib.h>
#include <chrono>
using namespace std;

#define EARTH_RADIUS 6378137.0
#define N_ELIPSE_POINTS 8
#define N_PATH_POINTS 32
#define PI 3.14159265359

class Timer
{
private:
	// Type aliases to make accessing nested type easier
	using clock_t = std::chrono::high_resolution_clock;
	using second_t = std::chrono::duration<double, std::ratio<1> >;

	std::chrono::time_point<clock_t> m_beg;

public:
	Timer() : m_beg(clock_t::now())
	{
	}

	void reset()
	{
		m_beg = clock_t::now();
	}

	double elapsed() const
	{
		return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
	}
};

class coordinates
{
private:
  float m_lat = 0;
  char m_NS = 'N';
  float m_lon = 0;
  char m_EW = 'E';
  float m_alt = 0;
  int m_index = 0;

  float getDistance(int x, int y)
  {
    return sqrt(pow(x, 2) + pow(y, 2));
  }

  float deg2rad(float alpha)
  {
    return alpha * PI / 180;
  }

  // This function is based on the get_distance_meters() function in this link: http://python.dronekit.io/examples/mission_basic.html
  coordinates getOffsetCoordinates(int dNorth, int dEast)
  {
    float dLat = (float)dNorth / EARTH_RADIUS;
    float dLon = (float)dEast / (EARTH_RADIUS * cos(PI * m_lat / 180));
    coordinates offsetLocation;
    offsetLocation.m_lat = m_lat + (dLat * 180 / PI);
    offsetLocation.m_lon = m_lon + (dLon * 180 / PI);
    offsetLocation.m_alt = m_alt;
    offsetLocation.m_index = m_index;
    offsetLocation.m_NS = m_NS;
    offsetLocation.m_EW = m_EW;
    return offsetLocation;
  }

  void createElipse(float *pointAngle, float *coordinateDistance, int a, int b)
  {
    for(int i = 0; i < N_ELIPSE_POINTS; i++)
    {
      float elipseAngle = deg2rad(360 * i / N_ELIPSE_POINTS);
      if (elipseAngle == deg2rad(90))
      {
        elipseAngle = deg2rad(89.99999);
      }else if(elipseAngle == deg2rad(270))
      {
        elipseAngle = deg2rad(270.00001);
      }
      coordinateDistance[i] = getDistance(a * cos(elipseAngle), b * sin(elipseAngle));
      //float pointAngle = 0;
      if (elipseAngle > deg2rad(90) && elipseAngle < deg2rad(270))
      {
        pointAngle[i] = atan((b * sin(elipseAngle)) / (a * cos(elipseAngle))) - deg2rad(180);
      }else
      {
        pointAngle[i] = atan((b * sin(elipseAngle)) / (a * cos(elipseAngle)));
      }
    }
   }

public:
  coordinates(){}
  coordinates (float lat, float lon, float alt)
  {
    m_lat = lat;
    m_lon = lon;
    m_alt = alt;
  }

  void printToBuffer(char* buffer)
  {
    //sprintf(buffer, "%i\t%f\t%c\t%f\t%c\t%f\n", m_index, m_lat, m_NS, m_lon, m_EW, m_alt);
    sprintf(buffer, "%f\t%f\n", m_lat, m_lon);
    //printf("%f\t%f\n", m_lat, m_lon);
  }

  // First we get the distance of every point of the elipse and then we rotate
  // each point the degrees corresponding to that distance + the offset angle.
  // void createElipse(coordinates *outputElipse, int a, int b, float alpha)
  // {
  //   for(int i = 0; i < N_ELIPSE_POINTS; i++)
  //   {
  //     //float pointAngle = deg2rad(360 * i / N_ELIPSE_POINTS);
  //     float pointAngle = deg2rad(90 * pow(i, 2) / pow(N_ELIPSE_POINTS, 2) + (i / N_ELIPSE_POINTS) * 90);
  //     float coordinateDistance = (a * b) / sqrt((pow(b, 2) - pow(a, 2)) * pow(cos(pointAngle), 2) + pow(a, 2));
  //     outputElipse[i] = getOffsetCoordinates(coordinateDistance * cos(pointAngle + deg2rad(alpha)), coordinateDistance * sin(pointAngle + deg2rad(alpha)));
  //     outputElipse[i].m_index = i;
  //   }
  // }

   void createPath(coordinates *outputPath, int a, int b, float alpha, int step)
   {
     float pointAngle[N_PATH_POINTS];
     float coordinateDistance[N_PATH_POINTS];
     createElipse(pointAngle, coordinateDistance, a, b);

     for(int i = 0; i < N_PATH_POINTS; i++)
     {
       if(i < N_ELIPSE_POINTS)
       {
         coordinateDistance[i] += step * i / N_ELIPSE_POINTS;
       }else
       {
         coordinateDistance[i] = coordinateDistance[i - N_ELIPSE_POINTS] + step;
         pointAngle[i] = pointAngle[i - N_ELIPSE_POINTS];
       }
       outputPath[i] = getOffsetCoordinates(coordinateDistance[i] * cos(pointAngle[i] + deg2rad(alpha)), coordinateDistance[i] * sin(pointAngle[i] + deg2rad(alpha)));
       outputPath[i].m_index = i;
     }
   }
};

void createSpiralMission(float lat, float lon, float alt)
{
	coordinates center(lat, lon, alt);

  // Get input parameters
  int elipseLong = 5;    // Long radius of ellipse in meters.
  int elipseShort = 5;  // Short radius of ellipse in meters.
  int elipseStep = 5;     // Distance between lines of the spiral.
  float elipseAngle = 90;  // Angle of the elipse in degrees from north clockwise.

  ofstream myfile;
  myfile.open("spiralWaypoints.txt");
  //Timer t;
  char toPrint[100];
  coordinates spiral[N_PATH_POINTS];
  center.createPath(spiral, elipseLong, elipseShort, elipseAngle, elipseStep);
  //printf("Time elapsed: %.8fs\n", t.elapsed());
  for (int i = 0; i < N_PATH_POINTS; i++)
  {
    spiral[i].printToBuffer(toPrint);
    myfile << toPrint;
  }
  myfile.close();
}

extern "C"
{
	void doCreateSpiralMission(float lat, float lon, float alt)
	{
		createSpiralMission(lat, lon, alt);
	}
}
