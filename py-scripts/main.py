from __future__ import print_function
try:
    from Rock_Client import *
    from adafruit_rockblock import RockBlock
except:
    print('Rockblock connection broken or local testing')

import serial
from multiprocessing import Process, Value
import pandas as pd
import json
import numpy as np

from config import *  #cambiar la ruta según sea necesario
from dronekit import *
import sys


if connectionString != "local":
    connection_string = flight_controller['port']
else:
    connection_string = None

sitl = None

if not connection_string:
    import dronekit_sitl
    sitl = dronekit_sitl.start_default()
    connection_string = sitl.connection_string()


baudrate = flight_controller['baudrate']
vehicle = connect(connection_string, baud=baudrate, wait_ready=True)


# Get some vehicle attributes (state)
cmds = vehicle.commands
cmds.download()
try:    
    rc = RockClient()
except: 
    print('No satellite, local testing')
    

#################################################################################

################################## Código #######################################

#################################################################################



vehicle.close()

# Shut down simulator
if sitl is not None:
    sitl.stop()
