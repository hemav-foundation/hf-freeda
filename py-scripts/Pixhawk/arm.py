from __future__ import print_function
import pandas as pd
import json
import numpy as np
from time import sleep
from time import time
from config import *
from commonFunctions import *
from dronekit import *
from Rock_Client import *
import time
import os
import sys
sys.path.insert(1, '../')
 
def armDrone(delta_time):

    previous = time.perf_counter()
    delta = delta_time

    vehicle.mode = VehicleMode("AUTO")
    vehicle.armed = True

    while not vehicle.armed:
        if(delta < 40):
            current = time.perf_counter()
            delta += current - previous
            sleep(1)
        else:
            print("false")
            break
    
    print("true")

if connectionString != "local":
    connection_string = flight_controller['port']
else:
    connection_string = None

sitl = None

# Start SITL if no connection string specified
if not connection_string:
    import dronekit_sitl
    sitl = dronekit_sitl.start_default()
    connection_string = sitl.connection_string()

vehicle = connect(connection_string, baud=flight_controller['baudrate'], wait_ready=True)


# Get some vehicle attributes (state)
global cmds
cmds = vehicle.commands

delta_time = 0

armDrone(delta_time)

# Close vehicle object before exiting script
vehicle.close()

# Shut down simulator
if sitl is not None:
    sitl.stop()


