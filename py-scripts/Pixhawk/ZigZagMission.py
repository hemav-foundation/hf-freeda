from __future__ import print_function
from math import asin,cos,pi,sin

#from dronekit import connect, VehicleMode, LocationGlobalRelative, LocationGlobal, Command
from dronekit import *
import time
import math
from pymavlink import mavutil
from commonFunctions import *
from config import *
from flights import *
import sys

distance = None
width = None
spaceDistance = None
periodDistance = None
height = None
inverse = True
headingOffset = None
latFlight = None
lonFlight = None
headingFlight = None

distance = float(sys.argv[1]) / 1000
width = float(sys.argv[2]) / 1000
spaceDistance = float(sys.argv[3]) / 1000
periodDistance = float(sys.argv[4]) / 1000
height = int(sys.argv[5])
headingOffset = int(sys.argv[7])
print(int(sys.argv[5]))

sitl = None

if(int(sys.argv[6]) == 1):
    inverse = True
else:
    inverse = False

if connectionString != "local":
    connection_string = flight_controller['port']
    latFlight = float(sys.argv[8])
    lonFlight = float(sys.argv[9])
    headingFlight = int(sys.argv[10])

else:
    connection_string = None
    latFlight = -35.363261
    lonFlight = 149.165229
    headingFlight = 353

#Start SITL if no connection string specified
if not connection_string:
    import dronekit_sitl
    sitl = dronekit_sitl.start_default()
    connection_string = sitl.connection_string()

global cmds
vehicle = connect(connection_string, baud=flight_controller['baudrate'], wait_ready=True) #objeto con el cual vamos a interactuar con la controladora del dron y nos va a dar datos
cmds = vehicle.commands # vehicle commands es donde vamos a ir registrando todos los puntos de control de vuelo (waypoints) donde finalmente se los pasaremos de nuevo a la controladora y esta sabrá que pasos ha de realizar para volar

latWind = vehicle.location.global_frame.lat #recogemos la latitud actual del drone
lonWind = vehicle.location.global_frame.lon #recogemos la longitud actual del drone
headingWind = vehicle.heading #recogemos el heading actual del drone

if inverse == False:
    cmds = ZigZagMission(latWind, lonWind, headingWind, distance, spaceDistance, periodDistance, width, height, headingOffset, latFlight, lonFlight, headingFlight, cmds)
else:
    cmds = ZigZagMissionInversed(latWind, lonWind, headingWind, distance, spaceDistance, periodDistance, width, height, headingOffset, latFlight, lonFlight, headingFlight, cmds)

print("New commands uploaded")

if connectionString == "local":
    save_mission('./mission.waypoints', cmds)

mission_values = {
    "distance": "",
    "width": "",
    "spaceDistance": "",
    "spaceLines": "",
    "height": "",
    "inverse": "",
    "headingOffset": "",
    "latFlight": "", 
    "lonFlight": "", 
    "headingFlight": "", 
    "missionParameters": "",
}

mission_values = edit_mission_values(mission_values, distance, spaceDistance, width, periodDistance, height, headingOffset, latFlight, lonFlight, headingFlight)
edit_mission_json(mission_values)

# Close vehicle object before exiting script
vehicle.close()

# Shut down simulator
if sitl is not None:
    sitl.stop()