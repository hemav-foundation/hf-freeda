from collections import deque
from imutils.video import VideoStream
import numpy as np
import argparse
import cv2
import imutils
import time
# python landing_video_kalman.py --video 1.mp4


ap = argparse.ArgumentParser()
ap.add_argument("-v", "--video",
	help="path to the (optional) video file")
ap.add_argument("-b", "--buffer", type=int, default=64,
	help="max buffer size")
args = vars(ap.parse_args())


greenLower = (74, 170, 0)
greenUpper = (102, 255, 255)
pts = deque(maxlen=args["buffer"])

vs = cv2.VideoCapture(args["video"])


# Initialisation of the Kalman Filter
kalman = cv2.KalmanFilter(4, 2)
kalman.measurementMatrix = np.array([[1, 0, 0, 0],                # Coordinate x
                                     [0, 1, 0, 0]], np.float32)   # Coordinate y

kalman.transitionMatrix = np.array([[1, 0, 1, 0],                 # x = x0 + vx*t
                                    [0, 1, 0, 1],                 # y = y0 + vy*t
                                    [0, 0, 1, 0],                 # vx = vx
                                    [0, 0, 0, 1]], np.float32)    # vy = vy

kalman.processNoiseCov = np.array([[1, 0, 0, 0],   # Constant noise that affects every parameter
                                   [0, 1, 0, 0],
                                   [0, 0, 1, 0],
                                   [0, 0, 0, 1]], np.float32) * 0.03

measurement = np.array((2, 1), np.float32) # Initialisation of the measurement matrix of the process of the Kalman Filter
prediction = np.zeros((2, 1), np.float32) # Initialisation of the prediction matrix of the process of the Kalman Filter

counter_flight = 0
area = 0
largest_area = 0
largest_area_index = 0
n_frame = 1

while True:
	if (n_frame % 20 == 0): # solo multiplos de 5

		frame = vs.read()

		frame = frame[1] if args.get("video", False) else frame

		if frame is None:
			break

		print n_frame

		frame = imutils.resize(frame, width=1200)

		height, width, channels = frame.shape
		w = width/2
		h = height/2
		prediction[0] = w
		prediction[1] = h

		blurred = cv2.GaussianBlur(frame, (11, 11), 0)
		hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)


		mask = cv2.inRange(hsv, greenLower, greenUpper)
		mask = cv2.erode(mask, None, iterations=1)
		mask = cv2.dilate(mask, None, iterations=6)

		cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
		cnts = imutils.grab_contours(cnts)
		center = None

		for c in cnts:
			M = cv2.moments(c)
			peri = cv2.arcLength(c, True)
			approx = cv2.approxPolyDP(c, 0.10 * peri, True)

			if len(approx) == 4:
				rect = cv2.minAreaRect(c)
				area_rect = rect[1][0] * rect[1][1]
				area = cv2.contourArea(c)

				if area_rect < 1.2 * area and area_rect > 0.8 * area:
					shape = 'rectangle'

				else:
					shape = 'blob'

			else:
				shape = 'blob'
				area = cv2.contourArea(c)

			if (M["m00"] == 0):
				M["m00"] = 1
			cX = int(M["m10"] / M["m00"])  # Obtaining of the x coordinate of the centre of the net
			cY = int(M["m01"] / M["m00"])  # Obtaining of the y coordinate of the centre of the net

			if shape == "rectangle" or shape == "blob":
				if area > largest_area and area > 100:
					largest_area = area
					largest_area_index = c
					cX_ok = cX
					cY_ok = cY
					# center[0] = cX_ok
					# center[1] = cY_ok

		if largest_area != 0:

			# find the largest contour in the mask, then use
			# it to compute the minimum enclosing circle and
			# centroid
			c = max(cnts, key=cv2.contourArea)
			((x, y), radius) = cv2.minEnclosingCircle(c)
			M = cv2.moments(c)
			center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
			cX_ok = center[0]
			cY_ok = center[1]

			# only proceed if the radius meets a minimum size
			if radius > 1:
				# draw the circle and centroid on the frame,
				# then update the list of tracked points
				cv2.circle(frame, (int(x), int(y)), int(radius),(0, 255, 255), 2)
				cv2.circle(frame, center, 5, (0, 0, 255), -1)
				cv2.drawContours(frame, [largest_area_index], -1, (255, 0, 255), 2)

			if counter_flight > 10:
				if abs(prediction[0] - cX_ok) < 300 and abs(prediction[1] - cY_ok) < 300:  # The Kalman Filter accepts the new point if it is close enough from the prediction made.
					cv2.putText(frame, "KALMAN ACCEPTABLE", (20, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 0),2)  # The values inside the function are related to the size of the text written, its location in the image, its colour, among other non-essential information.

				else:  # If the new point is not accepted by the Kalman Filter, the current point is not taken as a new reference.
					# The point from the previous step is taken still as the reference for the next step,
					# since the net has not been able to move far enough during the time interval between two consecutive frames.
					cv2.putText(frame, "KALMAN NOT ACCEPTABLE", (20, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.5,(0, 0, 255), 2)
					cX_ok = cX_prev
					cY_ok = cY_prev

			cv2.arrowedLine(frame, (w, h), (cX_ok, cY_ok), (255, 0, 0), 2)

			if abs(cX_ok - w) < 50 and (cY_ok - h) < 50:  # If the centre of the net is close enough to the centre of the image, an "OK" is drawn.
				cv2.putText(frame, "OK", (w, h), cv2.FONT_HERSHEY_SIMPLEX, 4, (255, 255, 255), 4)

			pts = [cX_ok, cY_ok]
			pts = np.int0(pts)
			pts = np.array([np.float32(pts[0]), np.float32(pts[1])], np.float32)

			# Correction of the Kalman Filter algorithm according to the new measurements entered.
			kalman.correct((pts));

			# New prediction from the Kalman Filter and drawing of the resulting values according on the image according to the update of the Kalman algorithm thanks to the new measurements.
			prediction = kalman.predict();
			frame = cv2.circle(frame, (prediction[0], prediction[1]), 5, (0, 255, 0), -1);

			# Update of previous coordinates: in the following step, the current coordinates will be the previous coordinates
			cX_prev = cX_ok
			cY_prev = cY_ok


		# Computation of the components of the vector from the centre of the image to the center of the net
		vector_centres = [cX_ok - w, cY_ok - h]
		alpha = math.atan2(vector_centres[0], -vector_centres[1])  # Range: [-pi, pi]; angle between the vector and the vertical plane that separates quadrants I and II
		alpha_deg = (alpha) * 180 / math.pi  # Conversion to degrees
		# print "Angle centre net: ", alpha_deg  # Value of the angle of the vector of the drone




		cv2.imshow("Frame", frame)
		key = cv2.waitKey(1) & 0xFF

		counter_flight = counter_flight + 1



		# if the 'q' key is pressed, stop the loop
		if key == ord("q"):
			break
	n_frame = n_frame + 1



# otherwise, release the camera
else:
	vs.release()

# close all windows
cv2.destroyAllWindows()