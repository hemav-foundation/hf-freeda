const { Router } = require("express");
var { PythonShell } = require("python-shell");
const router = Router();
const axios = require('axios')

const Config = require('./config');
const environment = Config.environment;

router.get("/options/arm/:mission", (req, res) => {
  const mission = req.params.mission;
  let options;

  if (environment == "drone" || environment == "linux") {
    options = {
      mode: "text",
      pythonPath: "/usr/bin/python3",
      pythonOptions: ["-u"], // get print results in real-time
      scriptPath: "./scripts"
    };
  } else if (environment == "win") {
    options = {
      mode: "text",
      pythonOptions: ["-u"], // get print results in real-time
      scriptPath: "./scripts"
    };
  } else {
    options = {
      mode: "text",
      pythonPath: "/usr/local/bin/python3",
      pythonOptions: ["-u"], // get print results in real-time
      scriptPath: "./scripts"
    };
  }


  PythonShell.run("arm.py", options, function (err, results) {
    //if (err) throw err;
    if (err) {
      res.status(400).send({ message: "ERROR: Fallo el script start.py" });
      console.log(err);
    } else {
      if (results[0] == "true") {
        axios.post("http://localhost:9000/api/start/options/fly/" + mission)
        .then(() => {
          console.log("OK");
        })
        .catch((err) => console.log(err));

        res.status(200).send({ message: "Drone armado" });
      }else{
        res.status(400).send({ message: "No se ha podido armar, ha tardado mas de 40 seg" });

      }
    }
  });

});

router.post("/options/fly/:mission", (req, res) => {
  const mission = req.params.mission;
  let options;

  if (environment == "drone" || environment == "linux") {
    options = {
      mode: "text",
      pythonPath: "/usr/bin/python3",
      pythonOptions: ["-u"], // get print results in real-time
      scriptPath: "./scripts",
      args: [
        mission,
      ]

    };
  } else if (environment == "win") {
    options = {
      mode: "text",
      pythonOptions: ["-u"], // get print results in real-time
      scriptPath: "./scripts",
      args: [
        mission,
      ]
    };
  } else {
    options = {
      mode: "text",
      pythonPath: "/usr/local/bin/python3",
      pythonOptions: ["-u"], // get print results in real-time
      scriptPath: "./scripts",
      args: [
        mission,
      ]
    };
  }


  PythonShell.run("start.py", options, function (err, results) {
    //if (err) throw err;
    if (err) {
      res.status(400).send({ message: "ERROR: Fallo el script start.py" });
      console.log(err);
    } else {
      res.status(200).send({ message: "Proceso de captura de imagenes iniciado" });
    }
  });

});

router.get("/options/calibration", (req, res) => {

  let options;

  if (environment == "drone" || environment == "linux") {
    options = {
      mode: "text",
      pythonPath: "/usr/bin/python3",
      pythonOptions: ["-u"], // get print results in real-time
      scriptPath: "./scripts"
    };
  } else if (environment == "win") {
    options = {
      mode: "text",
      pythonOptions: ["-u"], // get print results in real-time
      scriptPath: "./scripts"
    };
  } else {
    options = {
      mode: "text",
      pythonPath: "/usr/local/bin/python3",
      pythonOptions: ["-u"], // get print results in real-time
      scriptPath: "./scripts"
    };
  }


  PythonShell.run("calibration.py", options, function (err, results) {
    //if (err) throw err;
    if (err) {
      res.status(400).send({ message: "ERROR: Fallo el script calibration.py" });
      console.log(err);
    } else {
      res.status(200).send({ message: "Drone calibrado" });
    }
  });

});

module.exports = router;
