const { Router } = require("express");
const checkDiskSpace = require("check-disk-space");
const router = Router();
const fs = require("fs");
const { zip } = require("zip-a-folder");
const Config = require("./config");
const environment = Config.environment;
let LED = null;
let blikInterval = null;

try {
  const Gpio = require("onoff").Gpio;
  LED = new Gpio(21, "out");
} catch (error) {
  console.log("Local development");
}

function round(value, decimals) {
  return Number(Math.round(value + "e" + decimals) + "e-" + decimals);
}

function blinkLED() {
  //function to start blinking
  if (LED.readSync() === 0) {
    //check the pin state, if the state is 0 (or off)
    LED.writeSync(1); //set pin state to 1 (turn LED on)
  } else {
    LED.writeSync(0); //set pin state to 0 (turn LED off)
  }
}

router.get("/SDdiskSpace", (req, res) => {
  if (
    environment == "drone" ||
    environment == "linux" ||
    environment == "mac"
  ) {
    checkDiskSpace("/mnt").then((disk) => {
      res.status(200).send({
        freeSpace: round(disk.free / 1073741824, 2),
        sizeSpace: round(disk.size / 1073741824, 2),
      });
    });
  } else {
    checkDiskSpace("C:/").then((disk) => {
      res.status(200).send({
        freeSpace: round(disk.free / 1073741824, 2),
        sizeSpace: round(disk.size / 1073741824, 2),
      });
    });
  }
});

router.get("/USBdiskSpace", (req, res) => {
  if (
    environment == "drone" ||
    environment == "linux" ||
    environment == "mac"
  ) {
    checkDiskSpace("/dev/sda1").then((disk) => {
      res.status(200).send({
        freeSpace: round(disk.free / 1073741824, 2),
        sizeSpace: round(disk.size / 1073741824, 2),
      });
    });
  } else {
    checkDiskSpace("C:/").then((disk) => {
      res.status(200).send({
        freeSpace: round(disk.free / 1073741824, 2),
        sizeSpace: round(disk.size / 1073741824, 2),
      });
    });
  }
});

router.get("/USBconnected", (req, res) => {
  // res.status(200) if ok
  // res.status(400) if not connected
  if (
    environment == "drone" ||
    environment == "linux" ||
    environment == "mac"
  ) {
    checkDiskSpace("/dev/sda1").then((disk) => {
      res.status(200).send({});
    });
  } else {
    res.status(400).send({ mensaje: "No esta conectado el usb" });
  }
});

router.post("/loadData", async (req, res) => {
  res.status(200).send("Iniciando proceso");
  try {
    LED.writeSync(0); // Turn LED off
    blikInterval = setInterval(blinkLED, 750);
  } catch (error) {
    console.log("Local development");
  }

  setTimeout(async function () {
    let jsonSD = JSON.parse(fs.readFileSync("results.json"));
    console.log("json sd", jsonSD);
    let jsonUSB = JSON.parse(
      fs.readFileSync("/media/pi/DLOCUST-USB/dataUSBDrive.json")
    );
    console.log("json usb", jsonSD.dataOfFlights.length);

    for (let index = 0; index < jsonSD.dataOfFlights.length; index++) {
      console.log("11111");
      if (jsonSD.dataOfFlights[index].saved == false) {
        console.log("22222");

        let flight = jsonSD.dataOfFlights[index];
        console.log("voy a hacer un zip");
        await zip(
          "public/results/photos/" + flight.id_flight,
          "/media/pi/DLOCUST-USB/" + flight.id_flight + ".zip"
        ).then(() => {
          console.log("ZIP Done");
        });

        flight.saved = true;
        jsonUSB.dataOfFlights.push(flight);
      }
    }

    fs.writeFileSync("./results.json", JSON.stringify(jsonSD));
    fs.writeFileSync(
      "/media/pi/DLOCUST-USB/dataUSBDrive.json",
      JSON.stringify(jsonUSB)
    );
    try {
      endBlink(blikInterval);
    } catch (error) {
      console.log("Local development");
    }
  }, 10000);

  function endBlink(blink) {
    //function to stop blinking
    clearInterval(blink); // Stop blink intervals
    LED.writeSync(0); // Turn LED off
    LED.unexport(); // Unexport GPIO to free resources
  }
});

module.exports = router;
