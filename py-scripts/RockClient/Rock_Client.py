import time
from adafruit_rockblock import RockBlock
import serial
import formatter
import struct
import math
import json
from config import *
from commonFunctions import *


class RockClient():
    def __init__(self):

        self.rockblock_port = rockblock_settings['port']
        #self.rockblock_port = "/dev/ttyUSB0"
        self.baudrate = rockblock_settings['baudrate']
        self.serial_number = rockblock_settings['gcs_sn']
        self.rb = self.connect_rockblock()

    def check_connection(self):
        rb = self.rb
        resp = rb._uart_xfer("+CSQ")

        if resp[-1].strip().decode() == "OK":
            status = int(resp[1].strip().decode().split(":")[1])

        else:
            quality = False

        signal_strength = status

        print("Rockblock signal strength:", signal_strength)

        if signal_strength >= 1:
            quality = True

        else:
            quality = False

        return quality

    def connect_rockblock(self):
        # via USB cable
        print("Connected!")
        uart = serial.Serial(self.rockblock_port, self.baudrate)

        rb = RockBlock(uart)

        return rb

    def get_time(self, stampType):
        ''' Returns timestamp for flight ID and folder names or rockblock dateFormat for Backend'''
        # 1. get_time input: stampType = "id" (if you want to get timestamp for flight ID)
        # 2. get_time input: stampType = "dateFormat" (if you want to get timestamp for backend)

        rb = self.rb
        resp = rb._uart_xfer("+CCLK?")

        if resp[-1].strip().decode() == "OK":
            status = tuple(resp[1].decode().split(","))
            date = (status[0].split(":"))[1]
            tim = status[1]

            # this is not fully correct!
            year = str(int(date.split("/")[0]) + 2000)
            month = str(date.split("/")[1])
            day = str(date.split("/")[2])

            hour = str(tim.split(":")[0])  # UTC
            mins = str(tim.split(":")[1])
            sec = str(tim.split(":")[2]).rstrip("\n")

        if stampType == "id":  # 2020_05_14-15_12
            timestamp = year + "_" + month + "_" + day + "-" + hour + "_" + mins

        elif stampType == "dateFormat":  # 2011-05-14 15:12:41
            timestamp = year + "-" + month + "-" + day + " " + hour + ":" + mins + ":" + sec

        return timestamp

    def write_doma_message(self, status, param2, latitude, longitude, param5, param6, param7, param8, param9, param10, param11):
        # Short some of the common data:
        lat = nodecimals_coordinate(latitude)
        lon = nodecimals_coordinate(longitude)
        # id = drone_id.replace("-", "")
        id = drone_id

        # Select and construct message from status:
        data = ""

        if status == 0:  # LANDING
            # Identify and short rest of the data:
            heading = param5
            alt = round(param6)
            fltime = math.trunc(param7)

            # [drone_id, status, Lat, Lon, alt, fltime, heading]
            data = str(id) + ',' + str(status) + ',' + str(lat) + ',' + \
                str(lon) + ',' + str(alt) + ',' + \
                str(fltime) + ',' + str(heading)
            print(data)

        elif status == 1:  # FLIGHT
            # Identify and short rest of the data:
            heading = param5
            alt = round(param6)
            fltime = math.trunc(param7)

            # Codes for the mission type:
            missiontype = self.mission_coder(param2)

            # [drone_id, status, Lat, Lon, alt, fltime, heading, mission type]
            data = str(id) + ',' + str(status) + ',' + str(lat) + ',' + str(lon) + ',' + \
                str(alt) + ',' + str(fltime) + ',' + \
                str(heading) + ',' + str(missiontype)
            print(data)

        elif status == 2:  # MISSION
            # Identify and short rest of the data:
            heading = param5
            distance = math.trunc(param6 * 10)
            width = math.trunc(param7 * 10)

            # Short string of the timestamp:
            timestamp = date_coder(param8, "compressed")

            # Codes for the mission type:
            missiontype = self.mission_coder(param2)

            # [drone_id, status, type, homeLat, homeLon, heading, distance(km), *width(km)]
            data = str(id) + ',' + str(status) + ',' + str(missiontype) + ',' + str(lat) + ',' + str(
                lon) + ',' + str(heading) + ',' + str(distance) + ',' + str(width) + ',' + str(timestamp)
            print(data)

        elif status == 3:  # MISSION v2
            # Identify and short rest of the data:
            heading = param5
            distance = math.trunc(param6 * 1000)
            width = math.trunc(param7 * 1000)
            spaceDistance = math.trunc(param9 * 1000)
            spaceLines = math.trunc(param10 * 1000)
            headingOffset = param11

            # Short string of the timestamp:
            timestamp = date_coder(param8, "compressed")

            # Codes for the mission type:
            missiontype = self.mission_coder(param2)

            # [drone_id, status, type, homeLat, homeLon, heading, distance(km*1000), *width(km*1000), spaceDistance(km*1000), spaceLines(km*1000), headingOffset, missionId]
            data = str(id) + ',' + str(status) + ',' + str(missiontype) + ',' + str(lat) + ',' + str(lon) + ',' + str(heading) + \
                ',' + str(distance) + ',' + str(width) + ',' + str(spaceDistance) + \
                ',' +str(spaceLines) + ',' + str(headingOffset) + ',' + str(timestamp)
            print(data)

        return data

    def write_rb_message(self, msg, typ):
        # Write text message with correct prefix:
        sn = self.serial_number
        prefix = "RB" + str(sn) + str(drone_id)
        # Codification
        if typ is "rtl":
            # txt = RB01677HP2-087,3,Entered RTL mode
            prefix = prefix + ",3,"
        txt = prefix + str(msg)

        return txt

    def mission_coder(self, mission):
        # Codes for the message mission type:
        missiontype = None

        if mission == "rectangle":
            missiontype = 0

        elif mission == "zigzag":
            missiontype = 1

        elif mission == "straight":
            missiontype = 2

        return missiontype

    def send_Message(self, message):
        rb = self.rb
        cc = self.check_connection()

        previous = time.perf_counter()
        timer = 0

        while cc is not True:
            current = time.perf_counter()
            timer += current - previous
            previous = current
            cc = self.check_connection()
            print("Checking again...")

            if timer > 15:
                print('Im tired of checking signal')
                return [False, 0]

        if cc is not False:

            print("Ready to send message!")

            # put data in outbound buffer
            rb.text_out = message

            # try a satellite Short Burst Data transfer
            print("Talking to satellite...")

            status = rb.satellite_transfer()
            print("Try num:", 1)
            print("Satellite status:", status)
            MTQueue = self.buffer_status("MT")
            print("mt queue:", MTQueue)

            if(status[0] > 8):
                time.sleep(1)
                status = rb.satellite_transfer()
                print("Satellite status:", status)

                if(status[0] > 8):
                    print("The communication has failed")
                    return [False, 0]
                else:
                    # status = [MOcode, MOmsn, MTcode, MTmsn, MTlen, MTremaining]
                    print("\nDONE.")
                    return [True, MTQueue]
            else:
                # status = [MOcode, MOmsn, MTcode, MTmsn, MTlen, MTremaining]
                print("\nDONE.")
                return [True, MTQueue]

    def get_Message(self):
        print("Getting message...")
        rb = self.rb
        cc = self.check_connection()

        previous = time.perf_counter()
        timer = 0
        messages = []

        while cc is not True:
            current = time.perf_counter()
            timer += current - previous
            previous = current

            print("Checking again...")
            cc = self.check_connection()

            if timer > 15:
                print('Im tired of checking signal')
                break

        if cc is not False:
            messages = []
            # Check current MT buffer:
            MTQueue = self.buffer_status("MT")

            if MTQueue == 0:
                # try a satellite Short Burst Data transfer
                print("Talking to satellite...")
                status = rb.satellite_transfer()
                print("Try 1:", status)

                if status[0] > 8:
                    print("The communication has failed")
                else:
                    print("\nDONE.")
                    MTQueue = self.buffer_status("MT")

                if MTQueue == 0:
                    print("No messages received.")
                    messages = None

            while MTQueue >= 1:
                # get the text from the MT buffer
                message = rb.text_in
                messages.append(message)
                MTQueue = MTQueue - 1
                print("Remaining MT messages:", MTQueue)

            return messages

    def buffer_status(self, buffer):
        """Current state of the mobile originated and mobile terminated buffers"""
        rb = self.rb
        status = (None,) * 4  # [MOflag,MOmsn,MTflag,MTmsn]
        bufStatus = 0
        resp = rb._uart_xfer("+SBDS")
        if resp[-1].strip().decode() == "OK":
            status = resp[-3].strip().decode().split(":")[1]
            status = [int(s) for s in status.split(",")]
        status = tuple(status)
        print("BUFER STATUS:", status)
        if buffer is "MT":
            bufStatus = status[2]
        elif buffer is "MO":
            bufStatus = status[0]

        return int(bufStatus)

    def clear_buffer(self, deletetype):
        """ Clear the mobile originated buffer, mobile terminated buffer or both. """
        # Delete type:
        #     1. MO messages: 0
        #     2. MT messages: 1
        #     3. Both: 2

        rb = self.rb

        resp = rb._uart_xfer("+SBDD" + str(deletetype))
        res = int(resp[1].decode())

        if res == 0:
            print("Buffer(s) cleared succesfully.")
        else:
            print("An error occurred while clearing the buffer(s).")
