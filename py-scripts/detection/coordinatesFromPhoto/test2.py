'''
HEMAV FOUNDATION
10 / 05 / 2019
TEST 2

This test will start with a mission pre-upload on the Aircraft.
While the aircraft is executing the mission, if the target is located,
the mission will stop and will be saved into a file, then, the aircraft will
follow the target updating the setpoint coordinates on every loop with the ones
calculated of the target.
At one point, the pilot will trigger a switch and the Aircraft will start
doing a mission around the last position of the target in spiral during
a certain amount of time, when the aircraft finishes the mission, it will
re-upload the main mission and resume it at the current point.
'''
import ctypes
from ctypes import * # Used to use the C++ classes in python
import thread # Multi-threading library
import time
from dronekit import connect, VehicleMode, LocationGlobal, LocationGlobalRelative
from pymavlink import mavutil # Needed for command message definitions
import math
import constants
from coordinatesFromPhoto import coordinatesFromPhoto
from autoDef import detection

lib = ctypes.cdll.LoadLibrary('./libspiralPathCreation.so')

newDetection = 0
cX = 0
cY = 0

def readSpiral(aFileName):
    cmds = vehicle.commands
    missionlist=[]
    with open(aFileName) as f:
        for line in enumerate(f):
            linearray=line.split('\t')
            ln_index=int(linearray[0])
            ln_lat=int(linearray[1])
            ln_lon=int(linearray[2])
            ln_alt=int(linearray[3].strip())
            cmd = Command( 0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT_INT, mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, ln_lat, ln_lon, ln_alt)
            missionlist.append(cmd)
    return missionlist

def uploadMission(missionlist):
    """
    Upload a mission from a file.
    """
    #Clear existing mission from vehicle
    print(' Clear mission')
    cmds = vehicle.commands
    cmds.clear()
    #Add new mission to vehicle
    for command in missionlist:
        cmds.add(command)
    print(' Upload mission')
    vehicle.commands.upload()

def uploadSpiral(aFileName):
    missionlist = readSpiral(aFileName)
    print("\nUpload mission from a file: %s" % aFileName)
    #Clear existing mission from vehicle
    print(' Clear mission')
    cmds = vehicle.commands
    cmds.clear()
    #Add new mission to vehicle
    for command in missionlist:
        cmds.add(command)
    print(' Upload mission')
    vehicle.commands.upload()

def downloadMission():
    """
    Downloads the current mission and returns it in a list.
    It is used in save_mission() to get the file information to save.
    """
    print(" Download mission from vehicle")
    missionlist=[]
    cmds = vehicle.commands
    cmds.download()
    cmds.wait_ready()
    for cmd in cmds:
        missionlist.append(cmd)
    return missionlist

def saveMission(aFileName):
    """
    Save a mission in the Waypoint file format
    (http://qgroundcontrol.org/mavlink/waypoint_protocol#waypoint_file_format).
    """
    print("\nSave mission from Vehicle to file: %s" % aFileName)
    #Download mission from vehicle
    missionlist = downloadMission()
    #Add home location as 0th waypoint
    home = vehicle.home_location
    output="%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" % (0,1,0,16,0,0,0,0,home.lat,home.lon,home.alt,1)
    #Add commands
    for cmd in missionlist:
        commandline="%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n" % (cmd.seq,cmd.current,cmd.frame,cmd.command,cmd.param1,cmd.param2,cmd.param3,cmd.param4,cmd.x,cmd.y,cmd.z,cmd.autocontinue)
        output+=commandline
    with open(aFileName, 'w') as file_:
        print(" Write mission to file")
        file_.write(output)

def rad2deg(radians):
    return radians * 180.0 / math.pi

def deg2rad(degrees):
    return degrees * math.pi / 180.0

def mapArduino(x, in_min, in_max, out_min, out_max):
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min


def imageProcessingLoop():
    global newDetection, cX, cY
    while True:
        time.sleep(0.01)
        (cX, cY, newDetection) = detection()

connectionString = "/dev/ttyACM0"

print("Connecting to vehicle")
vehicle = connect(connectionString, wait_ready = True)


@vehicle.on_message('RC_CHANNELS')
def listener(self, name, message):
    global ch6, ch7
    ch6 = message.chan6_raw
    ch7 = message.chan7_raw
    if ch6 < 1500:
        sys.exit()


@vehicle.on_message('ATTITUDE')
def listener(self, name, message):
    global yawaRad, rollRad, pitchRad
    yawRad = message.yaw
    rollRad = message.roll
    pitchRad = message.pitch

time.sleep(2)

# Here we are flying but not using the image processing
while ch7 < 1500:
    time.sleep(0.5)

# Here we start the image processing
thread.start_new_thread(imageProcessingLoop, ())

while newDetection != 1:
    time.sleep(0.1)

# Here we have found the target and we save the mission and change to GUIDED mode
mainMission = downloadMission()
mainMissionNextWaypoint = vehicle.commands.next
vehicle.mode = VehicleMode("GUIDED")
time.sleep(0.5)

# Here we follow the target until the pilot hits the switch
while True:
    if (newDetection == 1):
        boatLocation = coordinatesFromPhoto(rad2deg(pitch), rad2deg(roll), yaw, cX, cY, vehicle.location.global_relative_frame)
        vehicle.simple_goto(boatLocation)
        newDetection = 0
    if(ch7 > 1500):
        break
    time.sleep(0.01)
# Here we create the spiral mission
lib.doCreateSpiralMission(vehicle.location.global_relative_frame.lat, vehicle.location.global_relative_frame.lon, vehicle.location.global_relative_frame.alt)
time.sleep(0.5)

# Here we upload the spiral mission
uploadSpiral("spiralWaypoints.txt")

# Here we start the new mission
vehicle.mode = VehicleMode("AUTO")

while vehicle.commands.next < 30:
    time.sleep(1)

# Here we resume the main mission
uploadMission(mainMission)
vehicle.commands.next = mainMissionNextWaypoint
