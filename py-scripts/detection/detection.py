from __future__ import division
import cv2
import numpy as np
from time import time
import math
from sklearn.cluster import DBSCAN
import os
import imutils

path = os.path.dirname(os.path.abspath(__file__))

img = cv2.imread('/Users/XavierBalaguer/iCloud Drive (archivado)/Desktop/MASTER/PAYLOAD/Data processing/A4.2/foto3.JPG')  # sacar foto, mirar desde raspi si no hace falta escribirla
original = cv2.imread('/Users/XavierBalaguer/iCloud Drive (archivado)/Desktop/MASTER/PAYLOAD/Data processing/A4.2/foto3.JPG')

altura = 50  # leer altura de la pixhawk
barco_min = 1  # se puede cambiar si la mision cambia para detectar personas
barco_max = 20


def areas(altura, barco_min, barco_max):
    from time import time
    tinicial = time()

    # OBJETIVO
    # Area objetivo (m2)
    area_metros_min = barco_min
    area_metros_max = barco_max

    # CAMARA
    # Sensor (mm)
    sx = 3.68
    sy = 2.76
    # Focal length of lens (mm)
    fl = 3.04
    # Pixels
    px = 1920
    py = 1080
    pixels_camera = px * py

    # Field of view wide (gra)
    HFOV = 62.2
    # HFOV = math.radians(HFOV)
    HFOVcal = 2 * math.atan(sx / (2 * fl))
    # HFOVcal = math.degrees(HFOVcal)
    # Field of view tall (gra)
    VFOV = 48.8
    # VFOV = math.radians(VFOV)
    VFOVcal = 2 * math.atan(sy / (2 * fl))
    # VFOVcal = math.degrees(VFOVcal)

    # GIMBAL angulo (grados)
    gimbal = 0
    gimbal = math.radians(gimbal)
    # falta otro eje gimbal

    h = altura

    if (h < 0):
        h = abs(h)
    if (int(h) == 0):
        h = 1

    # FOOTPRINT(m)
    fy2 = h * (math.tan(gimbal + 0.5 * VFOVcal) - math.tan(gimbal - 0.5 * VFOVcal))
    fx2 = h * (math.tan(gimbal + 0.5 * HFOVcal) - math.tan(gimbal - 0.5 * HFOVcal))
    footprint = fy2 * fx2

    area_pixels_min = area_metros_min * pixels_camera / footprint
    area_pixels_max = area_metros_max * pixels_camera / footprint

    porc_amin = round(area_pixels_min * 100 / pixels_camera, 3)
    disp_amin = 'Area min esperada: ' + repr(porc_amin) + '%'

    porc_amax = round(area_pixels_max * 100 / pixels_camera, 3)
    disp_amax = 'Area max esperada: ' + repr(porc_amax) + '%'

    tfinal = time()
    time = tfinal - tinicial

    return area_pixels_min, area_pixels_max, time


def color(img):
    from time import time
    tinicial = time()

    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    mask = cv2.inRange(hsv, (0, 20, 0), (255, 255, 255))  # azul (84, 0, 0), (134, 255, 255)
    # cv2.imshow("mask", mask)

    imask = mask > 0
    color = np.zeros_like(img, np.uint8)

    color[imask] = img[imask]
    img_color = color
    # cv2.imshow("blancos",img_color)

    tfinal = time()
    time = tfinal - tinicial

    return img_color, mask, time


def proc_cont(img, mask):
    from time import time
    tinicial = time()

    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    blur = cv2.GaussianBlur(gray, (5, 5), 0)

    # th, thresh = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
    # thresh = cv2.adaptiveThreshold(blur, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, 25, 8)
    ret2, thresh = cv2.threshold(blur, 127, 255, cv2.THRESH_BINARY)
    cv2.imshow("thr", thresh)

    kernel = np.ones((3, 3), np.uint8)  # funcion de la altura
    thresh = cv2.erode(thresh, kernel, iterations=5)
    thresh = cv2.dilate(thresh, kernel, iterations=5)  # al reves por _INV
    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    tfinal = time()
    time = tfinal - tinicial

    return contours, hierarchy, time


def size(contours, area_pixels_min, area_pixels_max):
    from time import time
    tinicial = time()

    areamin = []
    for i in contours:
        areamin.append(cv2.contourArea(i))
    j = [i for i in areamin if i >= area_pixels_min]
    pos2 = []
    for i in j:
        pos2.append(areamin.index(i))
    size_cont = [contours[i] for i in pos2]
    areamax = []
    for i in size_cont:
        areamax.append(cv2.contourArea(i))
    j2 = [i for i in areamax if i < area_pixels_max]
    pos3 = []
    for i in j2:
        pos3.append(areamax.index(i))
    size_cont = [size_cont[i] for i in pos3]

    tfinal = time()
    time = tfinal - tinicial

    return size_cont, time


def hierarchy_cont(contours, first_contours, hierarchy):
    from time import time
    tinicial = time()

    area_first = []
    area = []
    for i in first_contours:
        area_first.append(cv2.contourArea(i))
    for i in contours:
        area.append(cv2.contourArea(i))

    hierarchy = hierarchy[0]

    new_hierarchy = []
    for i in range(len(area)):
        index = area_first.index(area[i])
        new_hierarchy.append(hierarchy[index])
    new_hierarchy = np.array(new_hierarchy)

    parents = []
    for i in range(len(new_hierarchy)):
        if (new_hierarchy[i, 3] < 0):
            parents.append(contours[i])
    parents_cont = parents

    tfinal = time()
    time = tfinal - tinicial

    return parents_cont, time


def hull(contours):
    from time import time
    tinicial = time()

    hull = []
    for i in range(len(contours)):
        hull.append(cv2.convexHull(contours[i], False))
    hull_cont = hull

    tfinal = time()
    time = tfinal - tinicial

    return hull_cont, time


def centroids(contours):
    from time import time
    tinicial = time()

    coor = []
    if len(contours) == 0:
        tfinal = time()
        time = tfinal - tinicial
        return coor, time
    else:
        for i in range(len(contours)):
            M = cv2.moments(contours[i])
            cX = int(M["m10"] / M["m00"])
            cY = int(M["m01"] / M["m00"])
            coordi = (cX, cY)
            coor.append(coordi)

        tfinal = time()
        time = tfinal - tinicial
        return coor, time


def cluster(contours, centroids):
    from time import time
    tinicial = time()

    data = np.array(centroids)
    X = data

    # Compute DBSCAN
    # eps en funcion de la altura
    db = DBSCAN(eps=100, min_samples=3).fit(X)
    core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
    core_samples_mask[db.core_sample_indices_] = True
    labels = db.labels_

    k = -1  # noise
    class_member_mask = (labels == k)

    coor = X[class_member_mask & core_samples_mask]
    coor = X[class_member_mask & ~core_samples_mask]

    coor_list = []
    cluster_cont = []
    for i in range(len(coor)):
        cc = (coor[i][0], coor[i][1])
        coor_list.append(cc)
        index = centroids.index(coor_list[i])
        cluster_cont.append(contours[index])

    tfinal = time()
    time = tfinal - tinicial

    return coor_list, cluster_cont, X, labels, core_samples_mask, time


def circulo(img, contours):
    from time import time
    tinicial = time()

    for i in range(len(contours)):
        color_contours = (255, 0, 0)  # color for contours
        # cv2.drawContours(img, contours, i, color_contours, 2, 8)
        (x, y), radius = cv2.minEnclosingCircle(contours[i])
        center = (int(x), int(y))
        radius = int(radius) + 50
        circulo = cv2.circle(img, center, radius, (255, 0, 255), 5)

    tfinal = time()
    time = tfinal - tinicial
    return circulo, time



t_inicial = time()

altura = 50  # leer altura de la pixhawk
barco_min = 0.5  # se puede cambiar si la mision cambiar para detectar personas
barco_max = 50

(area_pixels_min, area_pixels_max, t_areas) = areas(altura, barco_min, barco_max)

(img_color, mask, t_color) = color(img)
(first_contours, hierarchy, t_proc_cont) = proc_cont(img_color, mask)
(size_cont, t_size) = size(first_contours, area_pixels_min, area_pixels_max)

(centroids, t_centroids) = centroids(size_cont)
(coor, cluster_cont, X, labels, core_samples_mask, t_cluster) = cluster(size_cont, centroids)

barco_min = 1
barco_max = 30

# (area_pixels_min, area_pixels_max, t_areas2) = areas(altura, barco_min, barco_max)
(contours2, t_size2) = size(cluster_cont, area_pixels_min * 1.5, area_pixels_max * 0.8)
# (contours, t_hierarchy) = hierarchy_cont(contours2, first_contours, hierarchy)

(target, t_circulo) = circulo(img, contours2)

t_final = time()

print('Tiempo areas: ' + repr(t_areas) + 's')
print('Tiempo proc_cont: ' + repr(t_proc_cont) + 's')
print('Tiempo size: ' + repr(t_size) + 's')
print('Tiempo centroids: ' + repr(t_centroids) + 's')
print('Tiempo cluster: ' + repr(t_cluster) + 's')
# print('Tiempo areas2: ' + repr(t_areas2) + 's')
print('Tiempo size2: ' + repr(t_size2) + 's')
# print ('Tiempo hierarchy: ' + repr(t_hierarchy) + 's')
print('Tiempo circulo: ' + repr(t_circulo) + 's')
print('Tiempo total: ' + repr(t_final - t_inicial) + 's')

print("Detected")

cv2.waitKey()


# cv2.imshow("img", target)
# cX_vector = zip(*coor)[0]
# cy_vector = zip(*coor)[1]
# detection = (cX_vector, cy_vector, 1)
#
#
#
#
# print(detection)

gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
blur = cv2.GaussianBlur(gray, (5, 5), 0)
thresh = cv2.adaptiveThreshold(blur, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, 29, 8)

drawing = np.zeros((thresh.shape[0], thresh.shape[1], 3), np.uint8)
for i in range(len(size_cont)):
    color_contours = (0, 0, 255)  # color for contours
    cv2.drawContours(drawing, size_cont, i, color_contours, 2, 1)
img_size_cont = drawing

drawing2 = np.zeros((thresh.shape[0], thresh.shape[1], 3), np.uint8)
for i in range(len(contours2)):
    color_contours = (0, 255, 255)  # color for contours
    cv2.drawContours(drawing2, contours2, i, color_contours, 2, 1)
img_size_cont2 = drawing2

drawing5 = np.zeros((thresh.shape[0], thresh.shape[1], 3), np.uint8)
# for i in range(len(contours)):
#     color_contours = (255, 0, 0)  # color for contours
#     cv2.drawContours(drawing5, contours, i, color_contours, 2, 1)
# img_parents_cont = drawing5

drawing3 = np.zeros((thresh.shape[0], thresh.shape[1], 3), np.uint8)
for i in range(len(first_contours)):
    color_contours = (0, 255, 0)  # color for contours
    cv2.drawContours(drawing3, first_contours, i, color_contours, 2, 1)
img_cont = drawing3

drawing4 = np.zeros((thresh.shape[0], thresh.shape[1], 3), np.uint8)
for i in range(len(cluster_cont)):
    color_contours = (130, 40, 200)  # color for contours
    cv2.drawContours(drawing4, cluster_cont, i, color_contours, 2, 1)
img_cluster = drawing4

resize = 300
original = imutils.resize(original, width=resize)
img_cont = imutils.resize(img_cont, width=resize)
img_size_cont = imutils.resize(img_size_cont, width=resize)
img_cluster = imutils.resize(img_cluster, width=resize)
img_size_cont2 = imutils.resize(img_size_cont2, width=resize)
target = imutils.resize(target, width=resize)

numpy_horizontal_top = np.hstack((original, img_cont, img_size_cont))
numpy_horizontal_bottom = np.hstack((img_cluster, img_size_cont2, target))

numpy_3x3 = np.vstack((numpy_horizontal_top, numpy_horizontal_bottom))

cv2.imshow("3x3", numpy_3x3)

# cv2.imshow("original", original)
# cv2.imshow("procesada", img_cont)
# cv2.imshow("size", img_size_cont)
# cv2.imshow("cluster", img_cluster)
# cv2.imshow("size2", img_size_cont2)
# cv2.imshow("target", target)

# cv2.imshow("hierarchy", img_parents_cont)


#

#
cv2.waitKey(0)
cv2.destroyAllWindows()
#
#
