import json
import matplotlib
# IMPORTANT to export figures w/o x-server
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import geopy.distance
import numpy as numpy
import time
from matplotlib.legend_handler import HandlerLine2D
import os
from matplotlib.lines import Line2D
from matplotlib.patches import Patch

class MapInterface():
    def __init__(self, timestamp):
        self.map_path = '/home/pi/Desktop/HF-DLOCUST/public/results/photos/' + str(timestamp) + '/vegetation_map.png'

    def open_json(self):
        with open(self.json_path, 'r+') as f:
            data = []
            try:
                data = json.load(f)
            except:
                print("Empty json r+")
            f.seek(0)
            f.close()
        return data['dataOfFlights']

    def get_flight(self, id, data): 
        flight = None
        for x in data:
            if x['date'] == id:
                flight = x
        return flight

    def get_quadrant(self, home_coordinates, results_coordinates):  # quadrant 1: +X, +Y; quadrant 2: +X, -Y; quadrant 3: -X, -Y; quadrant 4: -X, +Y
        if results_coordinates[0] > home_coordinates[0]:
            if results_coordinates[1] > home_coordinates[1]:
                quadrant = 1
            else:
                quadrant = 4
        if results_coordinates[0] < home_coordinates[0]:
            if results_coordinates[1] > home_coordinates[1]:
                quadrant = 2
            else:
                quadrant = 3
        return quadrant

    def get_relative_distances(self, home_coordinates, results_coordinates, quadrant):
        home_coordinates_x = (home_coordinates[0], results_coordinates[1])
        home_coordinates_y = (results_coordinates[0], home_coordinates[1])

        if quadrant == 1:
            distanceX = geopy.distance.distance(home_coordinates, home_coordinates_x).m
            distanceY = geopy.distance.distance(home_coordinates, home_coordinates_y).m
        if quadrant == 2:
            distanceX = geopy.distance.distance(home_coordinates, home_coordinates_x).m
            distanceY = geopy.distance.distance(home_coordinates, home_coordinates_y).m * -1
        if quadrant == 3:
            distanceX = geopy.distance.distance(home_coordinates, home_coordinates_x).m * -1
            distanceY = geopy.distance.distance(home_coordinates, home_coordinates_y).m * -1
        if quadrant == 4:
            distanceX = geopy.distance.distance(home_coordinates, home_coordinates_x).m * -1
            distanceY = geopy.distance.distance(home_coordinates, home_coordinates_y).m

        return distanceX, distanceY
    
    def main(self, flight_data):

        id_flight = flight_data['id_flight']
        data_flight = flight_data
        date_formatted = data_flight['date']

        if data_flight is not None: 
            home_coordinates = data_flight['homeCoordinates']
            results = data_flight['GreenResults']
        else:
            print('Flight not found')

        fig, ax = plt.subplots()
        ax.set_aspect(aspect='equal')

        ax.plot(0, 0, 'o', color='black', markersize = 6)

        plt.text(0, 0, 'HOME', ha='center', va='bottom', transform=ax.transData, wrap=True)

        legend_elements = [Line2D([0], [0], marker='o', color='w', label='Vegetation < 2 %',markerfacecolor='gray', markersize=6),
                   Line2D([0], [0], marker='o', color='w', label='Vegetation 2-7 %',markerfacecolor='g', markersize=6),
                   Line2D([0], [0], marker='o', color='w', label='Vegetation 7-15 %',markerfacecolor='yellow', markersize=6),
                   Line2D([0], [0], marker='o', color='w', label='Vegetation >15 %',markerfacecolor='red', markersize=6)] 

        ax.legend(handles=legend_elements, loc='upper center', bbox_to_anchor=(0.5, -0.3), fancybox=True )

        for x in results:
            quadrant = self.get_quadrant(home_coordinates, x['coordinates'])
            distanceX, distanceY = self.get_relative_distances(home_coordinates, x['coordinates'], quadrant)
            #print ('distance in x:', distanceX)
            #print ('distance in y:', distanceY)

            #time.sleep(2)

            if x['percentage'] < 2:
                ax.plot(distanceX, distanceY, 'o', color='gray', markersize = 1)
            if x['percentage'] >= 2 and x['percentage'] < 7:
                ax.plot(distanceX, distanceY, 'o', color='green', markersize = 3)
            if x['percentage'] >= 7 and x['percentage'] < 15:
                ax.plot(distanceX, distanceY, 'o', color='yellow', markersize = 5)
            if x['percentage'] > 15:
                ax.plot(distanceX, distanceY, 'o', color='red', markersize = 6)
                #plt.text(distanceX, distanceY, x['image_id'], ha='center', va='bottom', wrap=True)

        #label_format = '{:,.0f}'
        values_y = ax.get_yticks().tolist()
        
        ax.set_yticklabels((int(abs(x))) for x in values_y)


        values_x = ax.get_xticks().tolist()

        ax.set_xticklabels((int(abs(x))) for x in values_x)
        #ax.xaxis.set_major_locator(mticker.FixedLocator(values_x))
        plt.tight_layout(.1)
        ax.set_xlabel('Distance (m)', labelpad = 10)
        ax.set_ylabel('Distance (m)', labelpad = 10)
        ax.set_title(date_formatted, pad=20)

        plt.savefig(self.map_path, dpi=None, facecolor='w', edgecolor='w',
                orientation='portrait', papertype=None, format=None,
                transparent=False, bbox_inches='tight', pad_inches=0.3)
        