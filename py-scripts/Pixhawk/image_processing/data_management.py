import numpy as np
import sys
sys.path.insert(1,'../')
from config import *
import json
import reverse_geocoder as rg
import time
import pandas as pd
import math
import os

class DataManagement():
    def __init__(self, home_coordinates, timestamp, mission, date_formatted):
        self.mission_type = mission
        
        self.results = []
        self.visualimages = []
                
        self.flight_time = None
        self.flight_start = time.perf_counter()

        self.map_path = '/results/photos/' + str(timestamp) + '/vegetation_map.png'

        self.home_coordinates = home_coordinates
        self.total_images_processed = None  # this variable refers to the total number of vegetation images processed (not psoitive vegetation)
        self.region, self.country = self.location_decoder(home_coordinates)


        self.flight = {
            "id_plate": drone_id,
            "id_flight": timestamp,
            "typeOfFlight": mission,
            "missionParameters": "",
            "mapPath": self.map_path,
            "homeCoordinates": home_coordinates, 
            "country": self.country,
            "region": self.region,
            "date": date_formatted,
            "flightTime": self.flight_time,
            "saved": False,
            "totalImages": self.total_images_processed,
            "GreenResults": self.results,
            "VisualImages": self.visualimages,
        }
        

        self.flights_information = {
            "dataOfFlights": [],
        }


    def edit_json(self, new_flight, total_images_processed):

        flight_time = math.trunc(time.perf_counter() - self.flight_start)

        new_flight['flightTime'] = flight_time
        new_flight['totalImages'] = total_images_processed

        filename = "./scripts/mission_parameters.json" 
        mission_parameters = self.get_mission_parameteres(filename)   
        self.flight['missionParameters'] = mission_parameters   
        
        # we try to write an existing json. If not existing, we create a new one
        try:
            with open('/home/pi/Desktop/HF-DLOCUST/results.json', 'r+') as f:
                data = []
                try:
                    data = json.load(f)
                except:
                    print("Empty json r+")

                data['dataOfFlights'].append(new_flight)
                f.seek(0)
                json.dump(data, f, ensure_ascii=False)
                f.truncate()
                f.close()

        except:

            with open('/home/pi/Desktop/HF-DLOCUST/results.json', 'w+') as f:
               
                data = []
                try:
                    data = json.load(f)
                except:
                    print("Empty json x")

                f.seek(0)
                self.flights_information['dataOfFlights'].append(new_flight)
                json.dump(self.flights_information, f, ensure_ascii=False)
                f.truncate()
                f.close()
            os.chmod('/home/pi/Desktop/HF-DLOCUST/results.json', 0o777)
            #os.system('chmod a+w /home/pi/Desktop/HF-DLOCUST/results.json')
            
            #st = os.stat('/home/pi/Desktop/HF-DLOCUST/results.json')
            #os.chmod('/home/pi/Desktop/HF-DLOCUST/results.json', st.st_mode | stat.S_IWOTH)

        print("done")

    def write_json_vegetation(self, timestamp, num, percentage, data_drone, path):
        
        coordinates = (data_drone[0], data_drone[1])
        
        self.results.append(
            {
                "image_id": num,
                "percentage": percentage,
                "coordinates": coordinates,
                "image_path": path,
            }
        )

        return self.flight

    def write_json_visual(self, timestamp, num_visual, path_visual, coordinates):
        
        self.visualimages.append(
            {
                "image_id": num_visual,
                "coordinates": coordinates,
                "image_path": path_visual,
            }
        )

        return self.flight
    
    def location_decoder(self, coordinates):  # function to know the region and the country where the flight takes place
        
        location = rg.search(coordinates)

        df = pd.DataFrame.from_dict(location)
        region = df['name'][0]
        state = df['admin1'][0]
        country = df['cc'][0]
        with open('./scripts/image_processing/countries.json', 'r+') as f:
            countries = dict()
            countries = json.load(f)
            #print('countries list', countries)
            for x in countries:
                if x == country:
                    print('matched country code', x)
                    country = countries[x]
                    print('Country name:', country)
            f.close()

        return region, country

    def get_mission_parameteres(self, filename):
        mission_json = []
        try:
            with open(filename, "r+") as myfile:
                try:
                    mission_json = json.load(myfile) 
                    mission_parameters = str(mission_json['mission_info']['missionParameters'])
                    print('coded message writting json:', mission_parameters)
                except: 
                    mission_parameters = None
                return mission_parameters
                myfile.close()

        except FileNotFoundError:
            print("Error in loading mission_parameters")
            mission_parameters = None
            return mission_parameters

        
        



