
from image_processing.visual_camera_interface import *
from commonFunctions import *
from image_processing import main
import numpy as np
import os
import json
import pandas as pd
import cv2
import math
from config import *
import time

def main_loop_visual(timestamp, num, path, visualcamera_interface):
    img = visualcamera_interface.take_image()

    path_visual_json = '/results/photos/' + str(timestamp) + '/display_photos/' + str(num) + '.jpeg'
    
    visualcamera_interface.save_image(path, img, num)

    return 

path_mono, path_visual, raw_images, timestamp = main.create_directory()

num = 1 
visualcamera_interface = VisualCameraInterface(timestamp)

while num < 5:
    main_loop_visual(timestamp, num, path_visual, visualcamera_interface) 
    num += 1
    time.sleep(1)

