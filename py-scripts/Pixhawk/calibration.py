#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from dronekit import *
from commonFunctions import *
from config import *
import time
from image_processing.autopilot_interface import AutopilotInterface
from image_processing.visual_camera_interface import VisualCameraInterface
from image_processing.camera_interface import CameraInterface
try:
    from Rock_Client import RockClient
except:
    print('Satellite not imported')

import json

def check_accelerometers(status, armable, autopilot_interface):

    return status, armable

def check_compass(status, armable, autopilot_interface):
    max_offset = 400
    a = 0
    
    disabled = False

    compass1_values, compass1_type, compass1_use = autopilot_interface.get_compass_1()
    compass2_values, compass2_type, compass2_use = autopilot_interface.get_compass_2()
    compass3_values, compass3_type, compass3_use = autopilot_interface.get_compass_3()

    print('Compass 1 offsets:', compass1_values)
    print('Compass 2 offsets:', compass2_values)
    print('Compass 3 offsets:', compass3_values)

    for x in compass1_values:
        if x < 0:
            x = x*-1
        if x > max_offset: # wewant to detect any high value in compass offsets
            a += 1
            if compass1_type is 1: # compass external
                status['compass'][0] = "false"
                status['compass'][1] = "Drone needs compass recalibration"
                armable = False
            else:
                if compass1_use is 1: # this means compass is internal
                    disabled = autopilot_interface.disable_compass(1)  # if compass is internal 
                    a -= 1
    
    for x in compass2_values:
        if x < 0:
            x = x*-1
        if x > max_offset: # wewant to detect any high value in compass offse
            a += 1
            if compass2_type is 1: # compass external
                status['compass'][0] = "false"
                status['compass'][1] = "Drone needs compass recalibration"
                armable = False
            else:
                if compass2_use is 1: # this means compass is internal
                    disabled = autopilot_interface.disable_compass(2)
                    a -= 1
    
    for x in compass3_values:
        if x < 0:
            x = x*-1
        if x > max_offset: # wewant to detect any high value in compass offse
            print('compass use:', compass3_use)
            a += 1
            if compass3_type is 1:  # compass external
                status['compass'][0] = "false"
                status['compass'][1] = "Drone needs compass recalibration"
                armable = False
            else:
                if compass3_use is 1: # this means compass is internal
                    disabled = autopilot_interface.disable_compass(3)
                    a -= 1

    if a is 0:
        status['compass'][0] = "true"
        status['compass'][1] = "Drone ready"
        armable = True

    if disabled is True:
        status['compass'][0] = "false"
        status['compass'][1] = "One internal compass has been disabled. Maintenance recommended"
        armable = True
    else:
        compass_use = compass1_use + compass2_use + compass3_use
        if compass_use is not 3:
            status['compass'][0] = "false"
            status['compass'][1] = "One internal compass was previously disabled. Manteinance recommended"
            armable = True

    return status, armable


def check_battery(status, armable, autopilot_interface):
    voltage = autopilot_interface.get_voltage()
    if voltage < 25.4:
        status['battery'][0] = "false"
        status['battery'][1] = "Batteries not fully charged. Please, change them"
        status['battery'][2] = "البطاريات غير مشحونة بالكامل. من فضلك ، قم بتغييرها"
        armable = False
    else: 
        status['battery'][0] = "true"
        status['battery'][1] = "Batteries are ready to fly"
        status['battery'][2] = "البطاريات جاهزة للطيران"
    return status, armable

def check_ekf(status, armable, autopilot_interface):
    ekf = autopilot_interface.get_ekf()   # ekf is a boolean with its state
    if ekf is True: 
        status['ekf'][0] = "true"
        status['ekf'][1] = "EKF is OK to fly"
        status['ekf'][2] = "EKF على ما يرام للطيران"
    else:
        armable = False
        satellites = autopilot_interface.gps_info()
        if satellites < 8:
            status['ekf'][0] = "false"
            status['ekf'][1] = "Wait for GPS signal"
            status['ekf'][2] = "انتظر حتى تحصل إشارة GPS على EKF سليم"
        else:
            status['ekf'][0] = "false"
            status['ekf'][1] = "Please reboot drone"
            status['ekf'][2] = "خطأ EKF"

    return status, armable


def check_gps(status, armable, autopilot_interface):
    satellites = autopilot_interface.gps_info()
    print('Number of satellites:', satellites)
    if satellites >= 8:
        status['gps'][0] = "true"
        status['gps'][1] = "Good GPS signal"
        status['gps'][2] = "إشارة GPS جيدة"
    else:
        if satellites > 1:
            status['gps'][0] = "false"
            status['gps'][1] = "Not healthy GPS signal. Wait or reboot drone if necessary"
            status['gps'][2] = "إشارة GPS غير صحية. انتظر أو أعد تشغيل الطائرة بدون طيار إذا لزم الأمر"
            armable = False
        else:
            status['gps'][0] = "false"
            status['gps'][1] = "No GPS signal. Check sorroundings and reboot the drone if necessary"
            status['gps'][2] = "يرجى إعادة تشغيل الطائرة بدون طيار"
            armable = False

    return status, armable

def check_airspeed(status, armable, autopilot_interface):
    airspeed = autopilot_interface.get_airspeed()
    groundspeed = autopilot_interface.get_groundspeed()
    satellites = autopilot_interface.gps_info()

    if groundspeed < 0.2:
        if airspeed < 3:
            status['airspeed'][0] = "true"
            status['airspeed'][1] = "Airspeed calibration succeed"
            status['airspeed'][2] = "تنجح معايرة سرعة الهواء"
        else: 
            status['airspeed'][0] = "false"
            status['airspeed'][1] = "Calibration failed. Cover airspeed sensor and reapeat it"
            status['airspeed'][2] = "فشلت المعايرة. قم بتغطية مستشعر سرعة الهواء وكرره"
    else: 
        status['airspeed'][0] = "false"
        status['airspeed'][1] = "Calibration failed. Wait for GPS and repeat it"
        status['airspeed'][2] = "فشلت المعايرة. انتظر GPS وكررها"

    return status, armable

def check_rockblock(status, armable):
    connection = False
    try:
        satellite_interface = RockClient()
        rb = satellite_interface.connect_rockblock()
        connection = True
        status['satellite'][0] = "true"
        status['satellite'][1] = "OK"
        status['satellite'][2] = "حسنا"

    except: 
        status['satellite'][0] = "false"
        status['satellite'][1] = "Satellite wiring connection is broken"
        status['satellite'][2] = "اتصال الأسلاك الفضائية معطل"
        armable = False

    """ if connection is True:
        quality = satellite_interface.check_connection(rb)
        print('Rockblock signal:', quality)
        if quality is True:
            status['satellite'][0] = "true"
            status['satellite'][1] = "Good satellite signal"
            status['satellite'][2] = "إشارة فضائية جيدة"
        else:
            status['satellite'][0] = "false"
            status['satellite'][1] = "Not healthy satellite signal. Drone info will not be sent"
            status['satellite'][2] = "إشارة قمر صناعي غير صحية. لن يتم إرسال معلومات الطائرة بدون طيار"
            armable = False """

    return status, armable

def check_armable(status, armable, autopilot_interface):
    vehicle_armable = autopilot_interface.vehicle_armable()
    print('Is drone armable?', vehicle_armable)
    if armable is True and vehicle_armable is True:
        status['armable'][0] = "true"
        status['armable'][1] = "Have a nice flight"
        status['armable'][2] = "أتمنى لك رحلة سعيدة"
    else:
        if armable is True and vehicle_armable is False:
            status['armable'][0] = "false"
            status['armable'][1] = "Drone is not ready to fly. Try rebooting the drone and, if not, a recalibration will be needed"
            status['armable'][2] = "الطائرة بدون طيار ليست جاهزة للطيران. حاول إعادة تشغيل الطائرة بدون طيار ، وإذا لم يكن الأمر كذلك ، فستكون هناك حاجة إلى إعادة المعايرة"
        else:
            status['armable'][0] = "false"
            status['armable'][1] = "Drone is not ready to fly. Solve the previous failures"
            status['armable'][2] = "الطائرة بدون طيار ليست جاهزة للطيران. قم بحل الإخفاقات السابقة"

    return status

def check_spectral_camera(status, armable):
    try:
        camera_interface = CameraInterface()
        img = camera_interface.capture_frame()
        status['spectralCamera'][0] = "true"
        status['spectralCamera'][1] = "OK"
        status['spectralCamera'][2] = "حسنا"
        camera_interface.close_spectral_camera()
    except: 
        status['spectralCamera'][0] = "false"
        status['spectralCamera'][1] = "Check camera connector and reboot drone"
        status['spectralCamera'][2] = "تحقق من موصل الكاميرا وأعد تشغيل الطائرة بدون طيار"
        armable = False

    return status, armable

def check_visual_camera(status, armable):
    try:
        timestamp = '3333'
        visual_camera_interface = VisualCameraInterface(timestamp)
        img = visual_camera_interface.take_image()
        status['visualCamera'][0] = "true"
        status['visualCamera'][1] = "OK"
        status['visualCamera'][2] = "حسنا"
    except:
        status['visualCamera'][0] = "false"
        status['visualCamera'][1] = "Check camera connector and reboot drone"
        status['visualCamera'][2] = "تحقق من موصل الكاميرا وأعد تشغيل الطائرة بدون طيار"
        armable = False
    
    return status, armable

if(connectionString != "local"):
    connection_string = flight_controller['port']
else:
    connection_string = None
    
sitl = None

# Start SITL if no connection string specified
if not connection_string:
    import dronekit_sitl
    sitl = dronekit_sitl.start_default()
    connection_string = sitl.connection_string()

vehicle = None

while vehicle is None:
    vehicle = connect(connection_string, baud=flight_controller['baudrate'], wait_ready=True)

autopilot_interface = AutopilotInterface(vehicle)

status = {
    "accelerometers": ["true", "English comment", "Arab comment"],
    "compass": ["true", "English comment", "Arab comment"],
    "battery": ["true", "English comment", "Arab comment"],
    "ekf": ["true", "English comment", "Arab comment"],
    "gps": ["true", "English comment", "Arab comment"],
    "airspeed": ["true", "English comment", "Arab comment"],
    "satellite": ["true", "English comment", "Arab comment"],
    "spectralCamera": ["true", "English comment", "Arab comment"],
    "visualCamera": ["true", "English comment", "Arab comment"],
    "armable": ["true", "English comment", "Arab comment"],
}

armable = True


# Get some vehicle attributes (state)
#barometer and airspeed sensor calibration
autopilot_interface.airspeed_calibration()


status, armable = check_battery(status, armable, autopilot_interface)
status, armable = check_compass(status, armable, autopilot_interface)
status, armable = check_ekf(status, armable, autopilot_interface)
status, armable = check_gps(status, armable, autopilot_interface)
status, armable = check_rockblock(status, armable)
status, armable = check_spectral_camera(status, armable)
status, armable = check_visual_camera(status, armable)
status, armable = check_airspeed(status, armable, autopilot_interface)
status = check_armable(status, armable, autopilot_interface)

print("llego hasta aquí")

try:
    with open('./scripts/drone_status.json', 'r+') as f:
        data = []
        data = json.load(f)
        data['drone_status'] = status
        f.seek(0)
        json.dump(data, f, ensure_ascii=False)
        f.truncate()
        f.close()
except:
    with open('./scripts/drone_status.json', 'w') as f:
        data_json = {
            "drone_status": {},
        }
        data_json['drone_status'] = status
        f.seek(0)
        json.dump(data_json, f, ensure_ascii=False)
        f.truncate()
        f.close()


# Close vehicle object before exiting script
vehicle.close()

# Shut down simulator
if sitl is not None:
    sitl.stop()
