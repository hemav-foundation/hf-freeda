import constants
import math

class Coordinates:
    def __init__(self, lat, lon, alt):
        self.lat = lat
        self.lon = lon
        self.alt = alt

def get_location_metres(original_location, dNorth, dEast):
    """
    Returns a LocationGlobal object containing the latitude/longitude `dNorth` and `dEast` metres from the
    specified `original_location`. The returned Location has the same `alt` value
    as `original_location`.
    The function is useful when you want to move the vehicle around specifying locations relative to
    the current vehicle position.
    The algorithm is relatively accurate over small distances (10m within 1km) except close to the poles.
    For more information see:
    http://gis.stackexchange.com/questions/2951/algorithm-for-offsetting-a-latitude-longitude-by-some-amount-of-meters
    """
    earth_radius=6378137.0 #Radius of "spherical" earth
    #Coordinate offsets in radians
    dLat = dNorth/earth_radius
    dLon = dEast/(earth_radius*math.cos(math.pi*original_location.lat/180))

    #New position in decimal degrees
    newlat = original_location.lat + (dLat * 180/math.pi)
    newlon = original_location.lon + (dLon * 180/math.pi)
    return Coordinates(newlat, newlon, original_location.alt)


def coordinatesFromPhoto(pitch, roll, yaw, posX, posY, droneCoordinates):
    # The X coordinate must be the larger (in this case the 3280 pixel size).
    # X coordinate must be the related with the roll of the aircraft.
    # Roll and Pitch must be sent in degrees and with positive angles when
    # rolling to the right and pitching to the back.
    # The yaw angle starts at north and is clockwise. Must be sent in radians.

    # Range of degrees of every pixel
    pixelDegreeSizeX = constants.CAMERA_FOV_X / constants.IMAGE_PIXELS_X
    pixelDegreeSizeY = constants.CAMERA_FOV_Y / constants.IMAGE_PIXELS_Y

    # Position of the target in degrees from the left
    targetDegreePositionX = posX * pixelDegreeSizeX
    targetDegreePositionY = posY * pixelDegreeSizeY

    # See image to know where this expression comes from

    offsetDegreesX = (constants.CAMERA_FOV_X / 2.0) - targetDegreePositionX + roll
    offsetDegreesY = (constants.CAMERA_FOV_Y / 2.0) - targetDegreePositionY + pitch

    offsetRadiansX = (offsetDegreesX * math.pi) / 180.0
    offsetRadiansY = (offsetDegreesY * math.pi) / 180.0

    # A positive value means the target is on the right or on the front of the camera
    offsetPositionX = - droneCoordinates.alt * math.tan(offsetRadiansX) # meters
    offsetPositionY = droneCoordinates.alt * math.tan(offsetRadiansY) # meters

    dNorth = - offsetPositionX * math.sin(yaw) + offsetPositionY * math.cos(yaw)
    dEast = offsetPositionX * math.cos(yaw) + offsetPositionY * math.sin(yaw)
    boatLocation = get_location_metres(droneCoordinates, dNorth, dEast)

    return boatLocation
