from __future__ import print_function
try:
    from Rock_Client import *
    from adafruit_rockblock import RockBlock
except:
    print('Rockblock connection broken or local testing')

import serial
from multiprocessing import Process, Value
import cv2
import pandas as pd
import json
import numpy as np
import geopy.distance
from image_processing import main
from image_processing.data_management import DataManagement
from image_processing.visual_camera_interface import VisualCameraInterface
from image_processing.camera_interface import CameraInterface
from image_processing.autopilot_interface import AutopilotInterface
from image_processing.map_interface import MapInterface
from config import *
from commonFunctions import *
import os
import time
import datetime
from dronekit import *
import sys
sys.path.insert(1, '../')

#global flight_data

try:
    mission = str(sys.argv[1])
except:
    mission = 'rectangle'


if connectionString != "local":
    #print('connection string imported:', flight_controller['port'])
    connection_string = flight_controller['port']
else:
    connection_string = None

sitl = None


if not connection_string:
    import dronekit_sitl
    sitl = dronekit_sitl.start_default()
    connection_string = sitl.connection_string()


#print('baudrate imported:', flight_controller['baudrate'])
baudrate = flight_controller['baudrate']

connected = False

while connected is False:
    try:
        vehicle = connect(connection_string, baud=baudrate, wait_ready=True)
        connected = True
    except:
        print('Connection failed. I will try again')

print('#### connected ####')

# Get some vehicle attributes (state)
cmds = vehicle.commands
cmds.download()
try:
    rc = RockClient()
    rc.clear_buffer(2)
except:
    print('No satellite, local testing')

loiterMessage = False


def send_Mission(semibool, timestamp):
    # Send mission parameters
    if mission == "periscope":
        semibool.value = 0  # Periscope mission does not need to send messages
    else:
        mission_json = []
        try:
            with open('./scripts/mission_parameters.json', 'r+') as f:
                try:
                    mission_json = json.load(f)
                except:
                    print("Empty json r+")
                f.seek(0)
                f.close()

        except FileNotFoundError:
            print("Error in loading mission_parameters")

        m = mission_json['mission_info']

        if mission == "straight":
            width = 0
        else:
            width = m['width']

        if m['width'] is "":
            width = 0

        lat = m['latFlight']
        lon = m['lonFlight']
        heading = m['headingFlight']
        status = 3
        distance = m['distance']
        headingOffset = m['headingOffset']
        spaceDistance = m['spaceDistance']
        spaceLines = m['spaceLines']

        coded_message = rc.write_doma_message(
            status, mission, lat, lon, heading, distance, width, timestamp,spaceDistance,spaceLines, headingOffset)
        print('Coded message:', coded_message)
        res = rc.send_Message(coded_message)
        sended = res[0]

        with open('./scripts/mission_parameters.json', 'r+') as f:
            mission_json = json.load(f)
            mission_json['mission_info']['missionParameters'] = coded_message
            f.seek(0)
            json.dump(mission_json, f)
            f.truncate()
            f.close()

        while sended is False:
            semibool.value = 0
            res = rc.send_Message(coded_message)
            sended = res[0]

        if sended is True:
            semibool.value = 1


def send_Location(status, param2, latitude, longitude, param5, param6, param7, loc_pendingMessages):
    coded_message = rc.write_doma_message(
        status, param2, latitude, longitude, param5, param6, param7, None, None, None)
    res = rc.send_Message(coded_message)
    loc_pendingMessages.value = res[1]


def send_RockBlock(message, typ):  # Send from the drone to rb directly
    coded_message = rc.write_rb_message(message, typ)
    res = rc.send_Message(coded_message)


def get_Message(cmds, loiterWp):
    messages = rc.get_Message()
    while len(messages) > 0:
        print("Processing messages...")

        if messages[0].startswith("RB"):
            messages[0] = messages[0][9:]  # 'RB0016777RTL'

        if messages[0] == 'RTL':
            print("Entering RTL mode...")
            vehicle.mode = VehicleMode("RTL")
            while cmds.next < loiterWp:  # Wait until mode has changed
                print("Waiting for mode change...")
                time.sleep(1)

            coded_message = rc.write_rb_message(
                "Entered " + "RTL" + " mode", "rtl")
            res = rc.send_Message(coded_message)

            if res[0] == True:
                print("Succes at sending message!")
            else:
                print("No succes at sending message")
        else:
            print("no RTL received")

        messages.pop(0)


def check_Loiter(cmds, loiterMessage, totalWpCounted):
    try:
        # cmds.download()
        # cmds.wait_ready()
        if totalWpCounted is False:
            total = cmds.count
            totalWpCounted = True
        current = cmds.next
        print('Total waypoints:', total)
        print('Current waypoint:', current)
        if total is not 0:
            loiterWp = total - 5
            print('Loiter waypoint:', loiterWp)
            if current >= loiterWp:
                loiterMessage = True
    except:
        print('Wait ready failed')

    return loiterMessage, loiterWp


path_mono, path_visual, raw_images, timestamp = main.create_directory()

try:
    camera_interface = CameraInterface()
except:
    print('Spectral camera is not working')

autopilot_interface = AutopilotInterface(vehicle)

try:
    visualcamera_interface = VisualCameraInterface(timestamp)
except:
    print('Could not open visual camera')

# we get the home coordinates to introduce them in the intelligent RTL function
#global home_coordinates
home_coordinates = (autopilot_interface.get_latitude(),
                    autopilot_interface.get_longitude())
try:
    date_formatted = rc.get_time('dateFormat')  # 2011-05-14 15:12:41
    if date_formatted is None:
        d = datetime.datetime.now()
        # 2019-11-01 07:11:19
        date_formatted = str(
            d - datetime.timedelta(microseconds=d.microsecond))

except:
    d = datetime.datetime.now()
    # 2019-11-01 07:11:19
    date_formatted = str(d - datetime.timedelta(microseconds=d.microsecond))

data_interface = DataManagement(
    home_coordinates, timestamp, mission, date_formatted)
map_interface = MapInterface(timestamp)

global num
global num_visual


num = 1
num_visual = 1
total_images_processed = 0  # we want to count the total vegetation images processed

# Json structures containing all the data
flight_data = None

if connectionString != "local":
    altitudeCondition = 50
else:
    altitudeCondition = -50

# We initialize time variables for the visual camera
previous = time.perf_counter()
initTimer = time.perf_counter()
delta_time = 0
satellite_timer = 0
missionSended = False
loiterMessage = False
loc_pendingMessages = Value("i", 0)

totalWpCounted = False  # variable to fix the count of waypoints in check_loiter
loiterWp = 0  # needed to avoid failing on get message

print('Type of mission:', mission)

if mission in ["straight", "zigzag", "rectangle", "periscope"]:

    semibool = Value("i", 0)

    p1 = Process(target=send_Mission, args=(semibool, timestamp,))
    p1.start()

    while vehicle.armed is True and loiterMessage is False:

        print('Is vehicle armed?:', vehicle.armed)
        print('Vehicle heading', autopilot_interface.get_heading())
        altitude = autopilot_interface.get_altitude()
        current = time.perf_counter()
        delta_time += current - previous
        satellite_timer += current - previous
        print('Viual camera trigger:', delta_time)
        print('Satellite timer:', satellite_timer)
        previous = current

        if altitude >= altitudeCondition:
            if mission != "periscope":
                try:
                    flight_data, total_images_processed = main.main_loop_mono(
                        timestamp, num, total_images_processed, path_mono, raw_images, camera_interface, autopilot_interface, data_interface)
                    camera_interface.test_settings(num)
                    num += 1
                except:
                    print('Spectral camera was not working')
                    pass

            # we want to take images every 10 seconds
            if delta_time > visual_camera_settings['timer']:
                try:
                    flight_data = main.main_loop_visual(
                        timestamp, num_visual, path_visual, visualcamera_interface, autopilot_interface, data_interface)
                    num_visual += 1
                except:
                    print('Visual camera was not working')
                    pass
                delta_time = 0

        # we want to send location every 300 seconds
            if satellite_timer > rockblock_settings['message_timer']:

                if semibool.value is not 0 and mission is not "periscope":
                    if loc_pendingMessages.value is 0:
                        status = 1  # Flight status
                        current = time.perf_counter()
                        flight_time = current - initTimer
                        p2 = Process(target=send_Location, args=(status, mission, autopilot_interface.get_latitude(), autopilot_interface.get_longitude(
                        ), autopilot_interface.get_heading(), autopilot_interface.get_altitude(), flight_time, loc_pendingMessages))
                        p2.start()

                    else:
                        get_Message(cmds, loiterWp)

                satellite_timer = 0

            loiterMessage, loiterWp = check_Loiter(
                cmds, loiterMessage, totalWpCounted)

            # autopilot_interface.do_land_start(loiterWp)

    try:
        p1.kill()
        p2.kill()  # Send flight messages finished

    except:
        print('Satellite process has not even started')

    if flight_data is not None:
        try:
            data_interface.edit_json(flight_data, total_images_processed)
            print('json written')
        except:
            print('could not write json')
    else:
        print('Empty json')

    map_interface.main(flight_data)
    camera_interface.close_spectral_camera()

    if loiterMessage is True:
        print('Loiter has started')
        status = 0  # Landing status
        heading = 0  # Not necessary at landing
        satellite_timer = 0
        previous = time.perf_counter()
        # Mientras está armado envía mensaje cada landing timer
        while mission is not "periscope" and vehicle.armed is True:
            current = time.perf_counter()
            satellite_timer += current - previous
            previous = current

            if satellite_timer > rockblock_settings['landing_timer']:
                current_sat = time.perf_counter()
                flight_time = current_sat - initTimer
                send_Location(status, None, autopilot_interface.get_latitude(), autopilot_interface.get_longitude(
                ), autopilot_interface.get_heading(), autopilot_interface.get_altitude(), flight_time, loc_pendingMessages)
                satellite_timer = 0

vehicle.close()

# Shut down simulator
if sitl is not None:
    sitl.stop()
