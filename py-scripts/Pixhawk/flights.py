from commonFunctions import *
from dronekit import *
from config import *
from math import asin, cos, pi, sin
from math import *


def landing(latWind, lonWind, headingWind, cmds):
    landpoint = pointRadialDistance(latWind, lonWind, headingWind, 0.04)
    firstLandingWaypoint = pointRadialDistance(
        latWind, lonWind, (headingWind + 180), 0.1)
    secondLandingWaypoint = pointRadialDistance(
        firstLandingWaypoint.lat, firstLandingWaypoint.lon, (headingWind + 180), 0.1)
    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, latWind, lonWind, 80))
    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_NAV_LOITER_TIME, 0, 0, 180, 0, 80, 1, latWind, lonWind, 80))
    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_DO_LAND_START, 0, 0, 0, 0, 0, 0, latWind, lonWind, 80))

    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT, mavutil.mavlink.MAV_CMD_NAV_WAYPOINT,
                     0, 0, 0, 0, 0, 0, secondLandingWaypoint.lat, secondLandingWaypoint.lon, 50))
    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT, mavutil.mavlink.MAV_CMD_NAV_WAYPOINT,
                     0, 0, 0, 0, 0, 0, firstLandingWaypoint.lat, firstLandingWaypoint.lon, 40))
    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, landpoint.lat, landpoint.lon, 30))
    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_NAV_LAND, 0, 0, 0, 0, 0, 0, landpoint.lat, landpoint.lon, 30))


def takeoff(cmds):

    height = 50

    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_DO_SET_HOME, 0, 0, 1, 0, 0, 0, 0, 0, height))
    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_DO_SET_HOME, 0, 0, 1, 0, 0, 0, 0, 0, height))
    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_NAV_TAKEOFF, 0, 0, 20, 0, 0, 0, 0, 0, height))
    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_DO_CHANGE_SPEED, 0, 0, 0, 17, 0, 0, 0, 0, height))


def rectangleMission_reversed(latWind, lonWind, headingWind, distance, spaceDistance, widthRectangle, spaceBtwLines, height, headingOffset, latFlight, lonFlight, headingFlight, cmds):
    # we're going to calculate the mission, we need some space for the takeoff of the drone and this space will be 500 meters and the width of the rectangle in this case will be 500m
    fase = rad2deg(math.atan((widthRectangle/2)/(distance)))

    # h = (widthRectangle/2)/math.sin(deg2rad(fase))
    h = math.hypot(widthRectangle/2, distance)

    heightPoint = pointRadialDistance(
        latFlight, lonFlight, headingFlight, 0.3)  # 300 meters of take off to achive altitude
    spaceDistancePoint = pointRadialDistance(
        latFlight, lonFlight, headingFlight, spaceDistance)
    finalPoint = pointRadialDistance(
        spaceDistancePoint.lat, spaceDistancePoint.lon, headingFlight + headingOffset - fase, h)
    # firstLocation = pointRadialDistance(
    # latFlight, lonFlight, (headingFlight + fase), h)

    takeoff(cmds)

    # first point
    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, heightPoint.lat, heightPoint.lon, height))
    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, spaceDistancePoint.lat, spaceDistancePoint.lon, height))
    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, finalPoint.lat, finalPoint.lon, height))

    # second point
    locationLoop = pointRadialDistance(
        finalPoint.lat, finalPoint.lon, headingFlight + headingOffset + 90, widthRectangle)

    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, locationLoop.lat, locationLoop.lon, height))

    division = (distance / spaceBtwLines) / 2
    print(division)
    print(locationLoop.lat, locationLoop.lon)

    for x in range(round(division)):
        locationLoop = pointRadialDistance(
            locationLoop.lat, locationLoop.lon, headingFlight + headingOffset + 180, spaceBtwLines)
        cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                         mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, locationLoop.lat, locationLoop.lon, height))
        print(locationLoop.lat, locationLoop.lon)

        locationLoop = pointRadialDistance(
            locationLoop.lat, locationLoop.lon, headingFlight + headingOffset - 90, widthRectangle)
        cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                         mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, locationLoop.lat, locationLoop.lon, height))
        print(locationLoop.lat, locationLoop.lon)

        locationLoop = pointRadialDistance(
            locationLoop.lat, locationLoop.lon, headingFlight + headingOffset + 180, spaceBtwLines)
        cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                         mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, locationLoop.lat, locationLoop.lon, height))
        print(locationLoop.lat, locationLoop.lon)

        locationLoop = pointRadialDistance(
            locationLoop.lat, locationLoop.lon, headingFlight + headingOffset + 90, widthRectangle)
        cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                         mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, locationLoop.lat, locationLoop.lon, height))
        print(locationLoop.lat, locationLoop.lon)

    landing(latWind, lonWind, headingWind, cmds)

    cmds.upload()

    return cmds


def rectangleMission_normal(latWind, lonWind, headingWind, distance, spaceDistance, widthRectangle, spaceBtwLines, height, headingOffset, latFlight, lonFlight, headingFlight, cmds):

    heightPoint = pointRadialDistance(
        latFlight, lonFlight, headingFlight, 0.3)  # 300 meters of take off to achive altitude
    spaceDistancePoint = pointRadialDistance(
        latFlight, lonFlight, headingFlight, spaceDistance)
    finalPoint = pointRadialDistance(
        latFlight, lonFlight, headingFlight + headingOffset, distance)

    # we're going to calculate the mission, we need some space for the takeoff of the drone and this space will be 500 meters and the width of the rectangle in this case will be 500m
    fase = rad2deg(math.atan((widthRectangle/2)/0.3))
    print(fase)
    #h = (widthRectangle/2)/math.sin(deg2rad(fase))
    h = math.hypot(widthRectangle/2, 0.3)
    print(h)
    firstLocation = pointRadialDistance(
        latFlight, lonFlight, (headingFlight + headingOffset + fase), h)

    takeoff(cmds)

    # first point
    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, heightPoint.lat, heightPoint.lon, height))
    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, spaceDistancePoint.lat, spaceDistancePoint.lon, height))
    # cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
    # mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, firstLocation.lat, firstLocation.lon, height))

    print(finalPoint.lat, finalPoint.lon)
    print(firstLocation.lat, firstLocation.lon)

    # second point
    locationLoop = pointRadialDistance(
        spaceDistancePoint.lat, spaceDistancePoint.lon, headingFlight + headingOffset - 90, widthRectangle/2)
    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, locationLoop.lat, locationLoop.lon, height))

    division = (distance / spaceBtwLines) / 2
    print(division)
    print(locationLoop.lat, locationLoop.lon)

    for x in range(round(division)):
        locationLoop = pointRadialDistance(
            locationLoop.lat, locationLoop.lon, headingFlight + headingOffset, spaceBtwLines)
        cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                         mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, locationLoop.lat, locationLoop.lon, height))
        print(locationLoop.lat, locationLoop.lon)

        locationLoop = pointRadialDistance(
            locationLoop.lat, locationLoop.lon, headingFlight + headingOffset + 90, widthRectangle)
        cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                         mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, locationLoop.lat, locationLoop.lon, height))
        print(locationLoop.lat, locationLoop.lon)

        locationLoop = pointRadialDistance(
            locationLoop.lat, locationLoop.lon, headingFlight + headingOffset, spaceBtwLines)
        cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                         mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, locationLoop.lat, locationLoop.lon, height))
        print(locationLoop.lat, locationLoop.lon)

        locationLoop = pointRadialDistance(
            locationLoop.lat, locationLoop.lon, headingFlight + headingOffset - 90, widthRectangle)
        cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                         mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, locationLoop.lat, locationLoop.lon, height))
        print(locationLoop.lat, locationLoop.lon)

    landing(latWind, lonWind, headingWind, cmds)

    cmds.upload()

    return cmds


def straightMission(latWind, lonWind, headingWind, distance, spaceDistance, height, headingOffset, latFlight, lonFlight, headingFlight, cmds):

    heightPoint = pointRadialDistance(
        latFlight, lonFlight, headingFlight, 0.3)  # 300 meters of take off to achive altitude
    spaceDistancePoint = pointRadialDistance(
        latFlight, lonFlight, headingFlight, spaceDistance)
    finalPoint = pointRadialDistance(
        spaceDistancePoint.lat, spaceDistancePoint.lon, headingFlight + headingOffset, distance)
    returnFinalPoint = pointRadialDistance(
        finalPoint.lat, finalPoint.lon, headingFlight + headingOffset + 90, 0.3)
    returnHeightPoint = pointRadialDistance(
        returnFinalPoint.lat, returnFinalPoint.lon, headingFlight + headingOffset + 180, distance)

    takeoff(cmds)

    # first point
    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, heightPoint.lat, heightPoint.lon, height))
    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, spaceDistancePoint.lat, spaceDistancePoint.lon, height))
    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, finalPoint.lat, finalPoint.lon, height))
    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, returnFinalPoint.lat, returnFinalPoint.lon, height))
    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, returnHeightPoint.lat, returnHeightPoint.lon, height))

    landing(latWind, lonWind, headingWind, cmds)

    cmds.upload()

    return cmds


def periscopeMission(latWind, lonWind, headingWind, latFlight, lonFlight, cmds):

    radius = 0.3  # we need to have the radius variable in km
    wp_number = 10  # we define the number of wp we will have on one loop

    angle_between_wp = round((360 / wp_number), 1)

    takeoff(cmds)

    number_turns = 2 * 360

    counter_angle = 0

    while counter_angle <= number_turns:

        locationLoop = pointRadialDistance(
            latFlight, lonFlight, counter_angle, radius)
        cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                         mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, locationLoop.lat, locationLoop.lon, 300))

        counter_angle += angle_between_wp

    landing(latWind, lonWind, headingWind, cmds)

    cmds.upload()

    return cmds


def ZigZagMission(latWind, lonWind, headingWind, distance, spaceDistance, spaceBtwPeaks, width, height, headingOffset, latFlight, lonFlight, headingFlight, cmds):

    fase = rad2deg(math.atan((width/2)/0.3))
    hTakeOff = math.hypot((width/2), 0.3)

    faseDiagonal = rad2deg(math.atan(spaceBtwPeaks/width))
    hZigZag = math.hypot(width, spaceBtwPeaks)

    heightPoint = pointRadialDistance(
        latFlight, lonFlight, headingFlight, 0.3)
    spaceDistancePoint = pointRadialDistance(
        latFlight, lonFlight, headingFlight, spaceDistance)
    firstlocation = pointRadialDistance(
        heightPoint.lat, heightPoint.lon, (headingFlight + headingOffset + fase), hTakeOff)

    takeoff(cmds)

    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, latFlight, lonFlight, height))
    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, heightPoint.lat, heightPoint.lon, height))
    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, spaceDistancePoint.lat, spaceDistancePoint.lon, height))

    # Empezamos el zig-zag
    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, spaceDistancePoint.lat, spaceDistancePoint.lon, height))

    # Second point
    locationLoop = pointRadialDistance(
        spaceDistancePoint.lat, spaceDistancePoint.lon, headingFlight + headingOffset - (90-faseDiagonal), hZigZag/2)
    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, locationLoop.lat, locationLoop.lon, height))

    x = spaceBtwPeaks

    while x < distance:
        locationLoop = pointRadialDistance(
            locationLoop.lat, locationLoop.lon, headingFlight + headingOffset + (90-faseDiagonal), hZigZag)
        cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                         mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, locationLoop.lat, locationLoop.lon, height))

        locationLoop = pointRadialDistance(
            locationLoop.lat, locationLoop.lon, headingFlight + headingOffset - (90-faseDiagonal), hZigZag)
        cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                         mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, locationLoop.lat, locationLoop.lon, height))

        x = x + (2*spaceBtwPeaks)

    locationLoop = pointRadialDistance(
        locationLoop.lat, locationLoop.lon, headingFlight + headingOffset + 90, width/2)
    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, locationLoop.lat, locationLoop.lon, height))

    landing(latWind, lonWind, headingWind, cmds)

    cmds.upload()

    return cmds


def ZigZagMissionInversed(latWind, lonWind, headingWind, distance, spaceDistance, spaceBtwPeaks, width, height, headingOffset, latFlight, lonFlight, headingFlight, cmds):

    heightPoint = pointRadialDistance(
        latFlight, lonFlight, headingFlight, 0.3)  # 300 meters of take off to achive altitude
    spaceDistancePoint = pointRadialDistance(
        latFlight, lonFlight, headingFlight, spaceDistance)
    firstpoint = pointRadialDistance(
        spaceDistancePoint.lat, spaceDistancePoint.lon, headingFlight + headingOffset, distance)
    secondpoint = pointRadialDistance(
        firstpoint.lat, firstpoint.lon, headingFlight + headingOffset + 90, width/2)

    takeoff(cmds)

    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, heightPoint.lat, heightPoint.lon, height))
    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, spaceDistancePoint.lat, spaceDistancePoint.lon, height))
    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, firstpoint.lat, firstpoint.lon, height))
    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, secondpoint.lat, secondpoint.lon, height))

    faseDiagonal = rad2deg(math.atan(spaceBtwPeaks/width))
    hZigZag = math.hypot(width, spaceBtwPeaks)
    newHeadingFlight = headingFlight + 180
    locationLoop = pointRadialDistance(
        secondpoint.lat, secondpoint.lon, newHeadingFlight + headingOffset + (90-faseDiagonal), hZigZag)

    cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                     mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, locationLoop.lat, locationLoop.lon, height))

    x = spaceBtwPeaks

    while x < distance:
        locationLoop = pointRadialDistance(
            locationLoop.lat, locationLoop.lon, newHeadingFlight + headingOffset - (90-faseDiagonal), hZigZag)
        cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                         mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, locationLoop.lat, locationLoop.lon, height))

        locationLoop = pointRadialDistance(
            locationLoop.lat, locationLoop.lon, newHeadingFlight + headingOffset + (90-faseDiagonal), hZigZag)
        cmds.add(Command(0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT,
                         mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0, 0, 0, 0, locationLoop.lat, locationLoop.lon, height))

        x = x + (2*spaceBtwPeaks)

    landing(latWind, lonWind, headingWind, cmds)

    cmds.upload()

    return cmds
