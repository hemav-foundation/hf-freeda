#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from dronekit import *
from commonFunctions import *
from config import *
import time
import os

if(connectionString != "local"):
    connection_string = flight_controller['port']
else:
    connection_string = None

sitl = None

# Start SITL if no connection string specified
if not connection_string:
    import dronekit_sitl
    sitl = dronekit_sitl.start_default()
    connection_string = sitl.connection_string()

vehicle = None

while vehicle is None:
    vehicle = connect(connection_string,
                      baud=flight_controller['baudrate'], wait_ready=False)

# Get some vehicle attributes (state)
cmds = vehicle.commands

print(vehicle.heading)
print(vehicle.location.global_frame.lat)
print(vehicle.location.global_frame.lon)
print(vehicle.location.global_frame.alt)

# Close vehicle object before exiting script
vehicle.close()

# Shut down simulator
if sitl is not None:
    sitl.stop()

if(connectionString != "local"):
    usbPath = '/media/pi'

    usbList = os.listdir(usbPath)
    print('USB LIST:', usbList)

    usbLocust = 0

    for x in usbList:
        if x == 'DLOCUST-USB':
            usbLocust += 1

    if usbLocust > 1:
        exceedingUSBfolder = usbLocust - 1
        i = 0
        while i < exceedingUSBfolder:
            command = 'sudo rm -rf /media/pi/DLOCUST-USB'
            os.system(command)
            i += 1

    json_in_usb = os.listdir('/media/pi/DLOCUST-USB')
    json_existing = False
    for x in json_in_usb:
        if x == 'dataUSBDrive.json':
            json_existing = True

    if json_existing is False:
        print('json not found')
        for x in json_in_usb:
            if 'dataUSBDrive' in x:
                path_to_delete =  '/media/pi/DLOCUST-USB/' + x
                action = 'sudo rm -rf ' + path_to_delete
                os.system(action)

        json_backup_data = {
            "dataOfFlights": [],
        }
        with open('/media/pi/DLOCUST-USB/dataUSBDrive.json', 'w+') as f:
            f.seek(0)
            json.dump(json_backup_data, f, ensure_ascii=False)
            f.truncate()
            f.close()
            
        os.chmod('/media/pi/DLOCUST-USB/dataUSBDrive.json', 0o777)   

    
